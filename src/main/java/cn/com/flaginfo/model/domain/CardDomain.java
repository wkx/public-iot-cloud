package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardDomain {

    private Integer id;

    private String spId;

    private String simId;

    private String iccId;

    private String msiSdn;

    private String packageName;

    private String ratePlanName;

    private long activeDate;

    private long addDate;

    private String address;

    private Integer addressCode;

    private Integer groupId;

    private String keyWord;

    private String longitude;

    private String remark;

    private String status;

    private int synchronize;
}
