package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.ErrorFileDomain;
import cn.com.flaginfo.model.dto.ErrorFileListInputDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Mapper
public interface ErrorFileMapper {

    int findCount(ErrorFileListInputDTO dto);

    List<Map<String,Object>> findPage(ErrorFileListInputDTO dto);

    void save(ErrorFileDomain domain);

}
