package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.ReplyRecordDomain;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Mapper
public interface ReplyRecordMapper {

    void save(ReplyRecordDomain domain);

}
