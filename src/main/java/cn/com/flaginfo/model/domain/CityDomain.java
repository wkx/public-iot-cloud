package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/16.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CityDomain {

    private int id;

    private int cityId;

    private String name;

    private int level;
}
