package dev;

import com.alibaba.fastjson.JSONObject;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wkx on 2017/3/20.
 * @author wkx
 */
public class DevFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        //这里填写你允许进行跨域的主机ip
        httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        //允许的访问方法
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
        //Access-Control-Max-Age 用于 CORS 相关配置的缓存
        httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
        httpServletResponse.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        HttpSession session=httpServletRequest.getSession();
        if(session.getAttribute("sso_user")==null){
            Map<String,Object> info=new HashMap<>();
            Map<String,Object> userInfo=new HashMap<>();
            userInfo.put("id","16082310522310778277");
            userInfo.put("name","wkx");
            info.put("userInfo", JSONObject.toJSONString(userInfo));
            Map<String,Object> spInfo=new HashMap<>();
            spInfo.put("id","16050516421010006209");
            spInfo.put("spName","wkx");
            spInfo.put("spName","wkx");
            spInfo.put("platform","210000");
            spInfo.put("point","6");
            spInfo.put("amount","100");
            info.put("userInfo", userInfo);
            info.put("loginSpInfo", spInfo);
            session.setAttribute("sso_user",info);
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
