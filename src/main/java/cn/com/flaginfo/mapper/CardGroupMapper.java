package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.CardGroupDomain;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx 
 */
@Mapper
public interface CardGroupMapper {

    List<Map<String,Object>> findAll(@Param("spId") String spId);

    Map<String,Object> findByName(@Param("name") String name, @Param("spId") String spId);

    int save(CardGroupDomain domain);

    void update(CardGroupDomain domain);

    void delete(@Param("id") Integer id);
}
    

