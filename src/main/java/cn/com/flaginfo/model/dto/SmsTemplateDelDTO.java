package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SmsTemplateDelDTO {

    @NotNull(message = "ids不为空")
    private String ids;

}
