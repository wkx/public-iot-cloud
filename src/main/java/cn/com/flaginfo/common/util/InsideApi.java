package cn.com.flaginfo.common.util;

import cn.com.flaginfo.ApiClient;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
public class InsideApi {

    private static final Logger LOGGER= LoggerFactory.getLogger(InsideApi.class);

    public static JSONObject postForJson(String urlKey, String jsonBody){
        ApiClient client=new ApiClient(urlKey,"56713d7de4b099d61d9dccd7","1.0");
        client.setJsonBody(jsonBody);
        String result = client.postAsJson();
        return JSONObject.parseObject(result);
    }

}
