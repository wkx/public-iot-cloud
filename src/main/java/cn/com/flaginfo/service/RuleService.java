package cn.com.flaginfo.service;

import cn.com.flaginfo.Jasper.JasperClient;
import cn.com.flaginfo.model.dto.RuleListDTO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by wkx on 2017/3/10.
 *
 * @author wkx
 */
@Service
public class RuleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RuleService.class);

    private JSONObject generateReqParam(String property,String type,Object value,String id){
        JSONObject json=new JSONObject();
        json.put("property", property);
        json.put("type", type);
        json.put("value", value);
        json.put("id", id);
        return json;
    }

    private String generateSearchParam(RuleListDTO dto){
        String search;
        if(dto.getName()==null && dto.getStatus()== null)
            return null;
        JSONArray searchJson = new JSONArray();
        if (StringUtils.isNotBlank(dto.getName()))
            searchJson.add(generateReqParam("description","LIKE", dto.getName(),"description"));
        if (StringUtils.isNotBlank(dto.getStatus()))
            searchJson.add(generateReqParam("enabled","BOOLEAN_EQUALS", Boolean.parseBoolean(dto.getStatus()),"enabled"));
        search = searchJson.toJSONString();
        return search;
    }

    public String ruleList(HttpClient client, RuleListDTO dto) {
        String search=generateSearchParam(dto);
        return JasperClient.roleList(client, dto.getPageNo(), dto.getPageLimit(), search);
    }

    public void enableRule(HttpClient client,String id){
        JasperClient.enableRule(client,id);
    }

    public void disableRule(HttpClient client,String id){
        JasperClient.disableRule(client,id);
    }

    public void deleteRule(HttpClient client,String ids){
        JasperClient.deleteRule(client,ids);
    }

    public void createRule(HttpClient client,String code,String callbackUrl,String description,int acctId){
        JasperClient.createRule(client,code,callbackUrl,description,acctId);
    }

    public void updateRule(HttpClient client,int ruleId, String code,String callbackUrl, String description,String preJson){
        JasperClient.updateRule(client,ruleId,code,callbackUrl,description,preJson);
    }

    public JSONArray exportRuList(RuleListDTO dto, HttpClient client, HttpServletResponse response){
        String search=generateSearchParam(dto);
        String result=JasperClient.roleList(client, 1, 2, search);
        int totalCount=JSONObject.parseObject(result).getIntValue("totalCount");
        result=JasperClient.roleList(client,1,totalCount,search);
        return JSONObject.parseObject(result).getJSONArray("data");
    }
}
