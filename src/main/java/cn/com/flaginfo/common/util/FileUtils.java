package cn.com.flaginfo.common.util;

import cn.com.flaginfo.common.support.IotException;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wkx on 2017/3/15.
 *
 * @author wkx
 */
public class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    public static InputStream downLoadFile(String mediaId) {
        String url = "http://fastdfs.api.ums86.com:22133/media/download";
        HttpClient client = HttpClients.createDefault();
        List<NameValuePair> paramsList = new ArrayList<>();
        paramsList.add(new BasicNameValuePair("access_token", "token"));
        paramsList.add(new BasicNameValuePair("media_id", mediaId));
        paramsList.add(new BasicNameValuePair("submit", "download"));
        HttpPost post = new HttpPost(url);
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        try {
            post.setEntity(new UrlEncodedFormEntity(paramsList));
            HttpResponse response = client.execute(post);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK)
                throw new IotException("499", "文件下载失败");
            Header[] headers = response.getHeaders("Content-Disposition");
            if (headers == null || headers.length <= 0)
                throw new IotException("499", "文件下载失败");
            System.out.println("获取length:"+headers.length);
            if (!headers[0].getValue().contains("xlsx"))
                throw new IotException("499", "文件下载失败");
            return response.getEntity().getContent();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IotException("499", "文件下载失败");
        }
    }
    public static String uploadFile(InputStream inputStream,String fileName){
        String url="http://fastdfs.api.ums86.com:22133/media/uploadFile?access_token=token&type=image";
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("file",inputStream , ContentType.MULTIPART_FORM_DATA, fileName);
        builder.addTextBody("filename", fileName);
        httpPost.setEntity(builder.build());
        try {
            HttpResponse response=httpClient.execute(httpPost);
            if(response.getStatusLine().getStatusCode()!=HttpStatus.SC_OK)
                throw new IotException("499","文件上传失败");
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(),e);
            throw new IotException("499","文件上传失败");
        }
    }
}
