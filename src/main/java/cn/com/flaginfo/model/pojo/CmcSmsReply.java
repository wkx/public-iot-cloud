package cn.com.flaginfo.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CmcSmsReply {

    private String spId;

    private String mdn;

    private String content;

    private String keyWord;

    private String replyTime;

}
