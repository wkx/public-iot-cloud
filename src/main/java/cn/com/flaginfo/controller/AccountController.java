package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.model.domain.AccountDomain;
import cn.com.flaginfo.model.dto.AccountInputDTO;
import cn.com.flaginfo.service.AccountService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@RestController
@RequestMapping("/account")
public class AccountController extends BaseController{

    private final AccountService accountService;

    private static final Logger LOGGER= LoggerFactory.getLogger(AccountController.class);

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping("/authorize")
    public JsonView authentication(HttpSession session){
        JsonView jsonView= JsonView.getSuccView();
        String spId= SessionHolder.getSpId(session);
        jsonView.addAttribute("data",accountService.authorize(session,spId));
        return jsonView;
    }

    @RequestMapping("/bind")
    public JsonView bind(@Validated AccountInputDTO dto, BindingResult bindingResult,HttpSession session) {
        checkDTOParam(bindingResult);
        String spId= SessionHolder.getSpId(session);
        LOGGER.info("用户提交的绑定信息：{}", JSONObject.toJSONString(dto));
        AccountDomain domain=new AccountDomain();
        BeanUtils.copyProperties(dto,domain);
        AccountDomain result=accountService.bindAccount(session,spId,domain);
        JsonView jsonView=new JsonView();
        jsonView.addAttribute("data",result);
        return jsonView;
    }

}
