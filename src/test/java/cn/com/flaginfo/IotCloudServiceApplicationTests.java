package cn.com.flaginfo;


import cn.com.flaginfo.Jasper.JasperService;
import cn.com.flaginfo.Task.ImportTask;
import cn.com.flaginfo.common.util.FileUtils;
import cn.com.flaginfo.mapper.AccountMapper;
import cn.com.flaginfo.model.domain.AccountDomain;
import cn.com.flaginfo.model.pojo.CmcSmsReply;
import cn.com.flaginfo.model.pojo.SendToIccCard;
import cn.com.flaginfo.service.CardService;
import cn.com.flaginfo.service.CityService;
import cn.com.flaginfo.service.MessageService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IotCloudServiceApplicationTests {

	@Autowired
	private AccountMapper accountMapper;

	@Autowired
	private CardService cardService;

//	private HttpClient client(){
//		String username="chenbing";
//		String password="liangyan";
//		return JasperClient.getClient(username,password);
//	}

	@Autowired
	private JasperService jasperService;

	@Test
	public void testActive(){
		JSONArray jsonArray=new JSONArray();
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("simId",342445818);
		jsonObject.put("changeType",3);
		jsonObject.put("targetValue","DEACTIVATED_NAME");
		jsonObject.put("effectiveDate",null);

		JSONObject json=new JSONObject();
		json.put("simId",342443518);
		json.put("changeType",3);
		json.put("targetValue","DEACTIVATED_NAME");
		json.put("effectiveDate",null);
		jsonArray.add(json);
		jsonArray.add(jsonObject);

		//JasperClient.updateSim(client(),jsonArray);
	}

	@Test
	public void testUpwardMessage(){
		SendToIccCard sendToIccCard=new SendToIccCard();
		sendToIccCard.setContent("南京测试");
		sendToIccCard.setIccId("89860615010018343993");
		sendToIccCard.setApiUrl("https://api.10646.cn");
		sendToIccCard.setPassword("liangyan");
		sendToIccCard.setUsername("chenbing");
		sendToIccCard.setLicenseKey("62a675a0-2520-4d93-9111-678f2eed0922");
		jasperService.sendSms2Device(sendToIccCard);
	}

	@Autowired
	private MessageService messageService;

	@Test
	public void testReply(){
		CmcSmsReply reply=new CmcSmsReply();
		reply.setContent("分组测试");
		reply.setSpId("16050516421010006209");
		reply.setKeyWord("分组");
		reply.setMdn("18261949859");
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
		reply.setReplyTime(dateFormat.format(new Date()));
		AccountDomain domain=new AccountDomain();
		domain.setUsername("chenbing");
		domain.setPassword("liangyan");
		domain.setApiUrl("https://api.10646.cn");
		domain.setLicenseKey("62a675a0-2520-4d93-9111-678f2eed0922");
		messageService.forwardReply(reply,domain);
	}

	@Test
	public void testFileDownLoad(){
		InputStream inputStream=FileUtils.downLoadFile("020170315113340693296993");
		File file=new File("/Users/wkx/zhixun/temp/a.xlsx");
		try {
			FileOutputStream outputStream=new FileOutputStream(file);
			byte buffer[]=new byte[1024];
			int length;
			while((length=inputStream.read(buffer))!=-1){
				outputStream.write(buffer,0,length);
				outputStream.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("文件写入成功");
	}

	@Test
	public void testFileLoad(){
		File file=new File("/Users/wkx/zhixun/temp/a.xlsx");
		try {
			FileInputStream inputStream=new FileInputStream(file);
			String result=FileUtils.uploadFile(inputStream,"a.xlxs");
			System.out.println("文件上传成功："+result);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Autowired
	private ImportTask importTask;

	@Test
	public void testImportFile(){
		File file=new File("/Users/wkx/zhixun/temp/cardTemplate.xlsx");
		try {
			FileInputStream fileInputStream=new FileInputStream(file);
			importTask.importCard(fileInputStream,"16050516421010006209");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Autowired
	private CityService cityService;

	@Test
	public void testCity(){
		List<Map<String,Object>> list=cityService.data();
		System.out.println("获取的结果："+JSONObject.toJSONString(list));
	}

}
