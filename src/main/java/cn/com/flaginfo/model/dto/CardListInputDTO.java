package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardListInputDTO {

    private String spId;

    private String iccId;

    private String msiSdn;

    private String remark;

    private String keyWord;

    private String groupId;

    private int index;

    @Min(value = 1,message = "pageNo不能小于1")
    private int pageNo;
    @Min(value = 1,message = "pageLimit不能小于1")
    private int pageLimit;

}
