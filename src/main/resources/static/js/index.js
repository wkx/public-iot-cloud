/**
 * Created by Chuck on 17/3/9.
 */

var app = angular.module('index', ['ui.router','ui.bootstrap','mainController']);

app.config(function($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise("/account");
    $stateProvider.
    state('account', {
        url:'/account',
        controller: 'accountController',
        templateUrl: 'html/account.html'
    }).
    state('cardManage',{
        url:'/cardManage',
        controller: 'cardController',
        templateUrl: 'html/cardManage.html'
    }).
    state('phoneManage',{
        url:'/phoneManage',
        controller: 'phoneManageController',
        templateUrl: 'html/phoneManage.html'
    }).
    state('cardDetail',{
        url:'/cardDetail',
        controller: 'cardDetailController',
        templateUrl: 'html/cardDetail.html'
    }).
    state('cardFind',{
        url:'/cardFind',
        controller: 'cardFindController',
        templateUrl: 'html/cardFind.html'
    }).
    state('sendMessage',{
        url:'/sendMessage',
        controller: 'sendMessageController',
        templateUrl: 'html/sendMessage.html'
    }).
    state('charge',{
        url:'/charge',
        controller: '',
        templateUrl: 'html/charge.html'
    }).
    state('check',{
        url:'/check',
        controller: '',
        templateUrl: 'html/check.html'
    }).
    state('detail',{
        url:'/detail',
        controller: '',
        templateUrl: 'html/detail.html'
    }).
    state('errorData',{
        url:'/errorData',
        controller: 'errorDataController',
        templateUrl: 'html/errorData.html'
    })
});


//监听路由事件

app.run(['$rootScope', '$location' , '$state',function($rootScope, $location,$state){

    $rootScope.$on('$stateChangeStart',

        function(event, toState, toParams, fromState, fromParams){

            angular.element('#loading').hide();

        })

}]);



