package cn.com.flaginfo.service;

import cn.com.flaginfo.Jasper.JasperClient;
import cn.com.flaginfo.Jasper.JasperService;
import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.mapper.CardMapper;
import cn.com.flaginfo.model.domain.CardDomain;
import cn.com.flaginfo.model.dto.CardActiveInputDTO;
import cn.com.flaginfo.model.dto.CardListInputDTO;
import cn.com.flaginfo.model.dto.MapListDTO;
import cn.com.flaginfo.model.pojo.SendToIccCard;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/7.
 *
 * @author wkx
 */
@Service
public class CardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardService.class);

    private final CardMapper cardMapper;

    private final JasperService jasperService;

    @Autowired
    public CardService(CardMapper cardMapper, JasperService jasperService) {
        this.cardMapper = cardMapper;
        this.jasperService = jasperService;
    }

    private List<CardDomain> handleCard(JSONObject json,String spId,HttpClient client){
        List<CardDomain> list = new ArrayList<>();
        JSONArray data = json.getJSONArray("data");
        Map<String, Object> ratePlanMap = new HashMap<>();
        for (Object obj : data) {
            JSONObject $json = (JSONObject) obj;
            String ratePlanId = $json.getString("ratePlanId");
            String packageName;
            if (ratePlanMap.get(ratePlanId) != null)
                packageName = String.valueOf(ratePlanMap.get(ratePlanId));
            else{
                packageName = JasperClient.getPackageName(client, ratePlanId);
                ratePlanMap.put(ratePlanId, packageName);
            }
            CardDomain cardDomain=new CardDomain();
            cardDomain.setSpId(spId).setSimId($json.getString("simId"))
                    .setIccId($json.getString("iccid"))
                    .setMsiSdn($json.getString("msisdn"))
                    .setPackageName(packageName)
                    .setRatePlanName($json.getString("ratePlanName"))
                    .setActiveDate($json.getLongValue("activationDate"))
                    .setAddDate($json.getLongValue("dateAdded"))
                    .setStatus($json.getString("statusNameDisplay"));
            list.add(cardDomain);
        }
        LOGGER.info("物联网卡获取的数据量：{}",list.size());
        return list;
    }

    private CardDomain mergeMap(CardDomain source,CardDomain target){
        target.setAddress(source.getAddress()).setAddressCode(source.getAddressCode())
                .setGroupId(source.getGroupId()).setKeyWord(source.getKeyWord())
                .setLongitude(source.getLongitude()).setRemark(source.getRemark());
        return target;
    }

    @Transactional
    private void batchInsert(List<CardDomain> list,String spId){
        cardMapper.updateAllSynchronize(spId);
        for(CardDomain domain:list){
            CardDomain cardDomain=cardMapper.findByIccId(domain.getIccId());
            if(cardDomain==null){
                LOGGER.info("保存数据："+JSONObject.toJSONString(domain));
                cardMapper.save(domain);//数据第一次进行同步
            }
            else{
                cardMapper.delete(domain.getIccId());
                if(spId.equals(cardDomain.getSpId()))
                    cardMapper.save(mergeMap(cardDomain,domain));//物联网卡所属的企业未发生变化
                else
                    cardMapper.save(domain);//物联网卡所属的企业发生变化
            }
        }
        cardMapper.deleteAllSynchronize(spId);
    }

    @SuppressWarnings("unchecked")
    public void synchronizeCard(String spId,HttpClient client) {
        long beginTime = System.currentTimeMillis();
        String result = JasperClient.simList(client, null);
        JSONObject resultJson = JSON.parseObject(result);
        if (!resultJson.getBoolean("success"))
            throw new IotException("499", "物联网卡数据同步失败");
        int totalCount = resultJson.getIntValue("totalCount");
        resultJson = JSON.parseObject(JasperClient.simList(client, totalCount));
        if(!resultJson.getBoolean("success"))
            throw new IotException("499", "物联网卡数据同步失败");
        List<CardDomain> list=this.handleCard(resultJson,spId,client);
        System.out.println("请求耗时：" + (System.currentTimeMillis() - beginTime));
        this.batchInsert(list,spId);
        System.out.println("请求耗时：" + (System.currentTimeMillis() - beginTime));
    }

    public Map<String,Object> list(CardListInputDTO dto){
        Map<String,Object> result=new HashMap<>();
        int dataCount=cardMapper.finCount(dto);
        int index=(dto.getPageNo()-1)*dto.getPageLimit();
        dto.setIndex(index);
        result.put("dataCount",dataCount);
        result.put("list",cardMapper.findPage(dto));
        return result;
    }

    public Map<String,Object> cardDetail(HttpClient client,String simId) {
        String result= JasperClient.cardDetail(client,simId);
        JSONObject cardJson=JSONObject.parseObject(result);
        JSONArray dataJson=cardJson.getJSONArray("data");
        JSONObject json=dataJson.getJSONObject(0);
        Map<String,Object> cardDetail=new HashMap<>();
        cardDetail.put("acctManagementSystemId",json.getString("acctManagementSystemId"));
        String iccId=json.getString("iccid");
        cardDetail.put("iccId",iccId);
        cardDetail.put("msiSdn",json.getString("msisdn"));
        cardDetail.put("imsi1",json.getString("imsi1"));
        cardDetail.put("status",json.getString("statusNameDisplay"));
        cardDetail.put("online",json.getString("customerSuspendDisplay"));
        cardDetail.put("activeDate",json.getString("activationDate"));
        CardDomain domain=cardMapper.findByIccId(iccId);
        cardDetail.put("address",domain.getAddress());
        cardDetail.put("longitude",domain.getLongitude());
        return cardDetail;
    }

    @Transactional
    public void updateCard(CardDomain saveDomain){
        if(saveDomain.getKeyWord()!=null){
            LOGGER.info("修改物联网卡关键字不为空");
            CardDomain domain=cardMapper.findByKeyWord(saveDomain);
            if(domain!=null){
                LOGGER.info("该关键字已经存在,且被{}卡号绑定",domain.getIccId());
                if(!saveDomain.getIccId().equals(domain.getIccId()))
                    throw new IotException("499","该关键字已经存在");
            }
        }
        cardMapper.update(saveDomain);
    }

    public void activeCard(HttpClient client, CardActiveInputDTO dto){
        String simIdList=String.valueOf(dto.getSimIdList());
        JSONArray simIdJsonArray=JSONArray.parseArray(simIdList);
        //当更改SIM卡状态时，该字段的值为3，当变更资费计划时，该字段的值为4
        //停用：DEACTIVATED_NAME 已经激活：ACTIVATED_NAME  INVENTORY_NAME---库存 ACTIVATION_READY_NAME---可激活 RETIRED_NAME---已失效
        for(Object obj:simIdJsonArray){
            JSONObject json= (JSONObject) obj;
            json.put("changeType",dto.getChangeType());
            json.put("targetValue",dto.getTargetValue());
            json.put("effectiveDate",null);
        }
        LOGGER.info("请求参数：{}",simIdJsonArray.toString());
        JasperClient.updateSim(client,simIdJsonArray);
    }

    public void upwardMessage(SendToIccCard sendToIccCard){
        jasperService.sendSms2Device(sendToIccCard);
    }

    public List<Map<String,Object>> mapList(MapListDTO dto){
        int index=(dto.getPageNo()-1)*dto.getPageLimit();
        dto.setIndex(index);
        return cardMapper.findPageByMap(dto);
    }

    public List<Map<String,Object>> exportIccId(CardDomain domain){
        return cardMapper.findAll(domain);
    }

}
