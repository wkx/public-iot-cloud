package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class PhoneDomain {

    private int id;

    private String username;

    private String phone;

    private String identityId;

    private String address;

    private String iccId;
}
