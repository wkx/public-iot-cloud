package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.AccountDomain;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx
 */
@Mapper
public interface AccountMapper {

    AccountDomain findBySpId(@Param("spId")String spId);

    AccountDomain findByUsername(@Param("username")String username);

    void save(AccountDomain domain);
}
