package cn.com.flaginfo.common.support;

import lombok.Data;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@Data
public class IotException extends RuntimeException{

    private Exception e;

    private String message;

    private String code;

    public IotException(){}

    public IotException(String code,String message){
        this.message=message;
        this.code=code;
    }

    public IotException(String message,Exception e){
        this.e=e;
        this.message=message;
        this.code="499";
    }

}
