package cn.com.flaginfo.service;

import cn.com.flaginfo.mapper.ErrorFileMapper;
import cn.com.flaginfo.model.dto.ErrorFileListInputDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Service
public class ImportService {

    private final ErrorFileMapper errorFileMapper;

    @Autowired
    public ImportService(ErrorFileMapper errorFileMapper) {
        this.errorFileMapper = errorFileMapper;
    }

    public Map<String,Object> errorFileList(ErrorFileListInputDTO dto){
        int index=(dto.getPageNo()-1)*dto.getPageLimit();
        dto.setIndex(index);
        int count=errorFileMapper.findCount(dto);
        List<Map<String,Object>> list=errorFileMapper.findPage(dto);
        Map<String,Object> result=new HashMap<>();
        result.put("dataCount",count);
        result.put("list",list);
        return result;
    }


}
