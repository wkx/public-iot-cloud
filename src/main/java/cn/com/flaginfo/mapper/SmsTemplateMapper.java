package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.SmsTemplateDomain;
import cn.com.flaginfo.model.dto.SmsTemplateUpdateDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Mapper
public interface SmsTemplateMapper {

    void save(SmsTemplateDomain domain);

    List<Map<String,Object>> findLikeInstruct(@Param("spId")String spId,@Param("instruct")String instruct);

    SmsTemplateDomain findByInstruct(@Param("spId")String spId,@Param("instruct")String instruct);

    void update(SmsTemplateUpdateDTO dto);

}
