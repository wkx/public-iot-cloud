package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class PhoneSaveDTO {

    @NotNull(message = "iccId不可为空")
    private String iccId;

    @NotNull(message = "phone不可为空")
    private String phone;

    @NotNull(message = "username不可为空")
    private String username;

    private String identityId;

    private String address;

}
