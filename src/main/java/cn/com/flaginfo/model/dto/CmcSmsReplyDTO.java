package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CmcSmsReplyDTO {
	@NotNull
	private String spId;
	@NotNull
	private String mdn;
	@NotNull
	private String text;
	@NotNull
	private String replyTime;
}
