package cn.com.flaginfo.config;

import cn.com.flaginfo.common.util.SystemMessage;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Configuration
@SuppressWarnings("unchecked")
public class RedisConfig extends CachingConfigurerSupport {

    @Bean
    public RedisConnectionFactory redisConnectionFactory(){
        JedisPoolConfig poolConfig=new JedisPoolConfig();
        poolConfig.setMaxIdle(SystemMessage.getInteger("spring.redis.pool.max-idle"));
        poolConfig.setMinIdle(SystemMessage.getInteger("spring.redis.pool.min-idle"));
        poolConfig.setMaxWaitMillis(SystemMessage.getInteger("spring.redis.pool.max-wait"));
        poolConfig.setMaxTotal(SystemMessage.getInteger("spring.redis.pool.max-active"));
        JedisConnectionFactory redisConnectionFactory=new JedisConnectionFactory();
        redisConnectionFactory.setHostName(SystemMessage.getString("spring.redis.host"));
        redisConnectionFactory.setPassword(SystemMessage.getString("spring.redis.password"));
        redisConnectionFactory.setPort(SystemMessage.getInteger("spring.redis.port"));
        redisConnectionFactory.setUsePool(true);
        redisConnectionFactory.setPoolConfig(poolConfig);
        redisConnectionFactory.setUsePool(true);
        return redisConnectionFactory;
    }

    @Bean
    @Primary
    public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory factory) {
        StringRedisTemplate template = new StringRedisTemplate(factory);
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        return template;
    }
}
