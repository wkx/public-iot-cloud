package cn.com.flaginfo.common.util;

import com.taobao.diamond.manager.DiamondManager;
import com.taobao.diamond.manager.ManagerListener;
import com.taobao.diamond.manager.impl.DefaultDiamondManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Executor;

/**
 * 配置 1.先加载本地配置文件 2.如果配置需要从远程读取,那么获取远程文件配置覆盖掉本地配置
 * 
 * @author Rain
 *
 */
public class SystemMessage {

	private final static Logger LOGGER = LoggerFactory
			.getLogger(SystemMessage.class);
	private static Properties properties = new Properties();

	// 本地配置文件
	private static final String BUNDLE_NAME = "application";

	static {
		// 全部从服务器读取配置文件
		ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);
		properties = new Properties();
		DiamondManager manager = new DefaultDiamondManager(
				bundle.getString("prop_group"),
				bundle.getString("prop_data_id"), new ManagerListener() {
			public void receiveConfigInfo(String configInfo) {
				properties.putAll(getPropertiesForString(configInfo));
			}
			public Executor getExecutor() {
				return null;
			}
		});
		properties.putAll(manager.getAvailablePropertiesConfigureInfomation(5000));
	}

	private static Properties getPropertiesForString(String configInfo) {
		Properties p = new Properties();
		try {
			p.load(new StringReader(configInfo));
			if (LOGGER.isDebugEnabled()) {
				Set<Entry<Object, Object>> set = properties.entrySet();
				for (Entry e : set) {
					LOGGER.debug(e.getKey() + "==>" + e.getValue());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}

	public static Map getAllConfig() {
		Set<Entry<Object, Object>> set = properties.entrySet();
		Map<Object,Object> allMap = new HashMap<>();
		for (Entry e : set) {
			allMap.put(e.getKey(), e.getValue());
		}
		return allMap;
	}

	public static String getString(String key) {
		return properties.getProperty(key);
	}

	public static Integer getInteger(String key) {
		String value = getString(key);
		return Integer.parseInt(value.trim());
	}

	public static Long getLong(String key) {
		String value = getString(key);
		try {
			return Long.valueOf(value);
		} catch (Exception e) {
			return null;
		}
	}

	public static Double getDouble(String key) {
		String value = getString(key);
		try {
			return Double.valueOf(value);
		} catch (Exception e) {
			return null;
		}
	}

	public static Float getFloat(String key) {
		String value = getString(key);
		try {
			return Float.valueOf(value);
		} catch (Exception e) {
			return null;
		}
	}

	public static Boolean getBoolean(String key) {
		String value = getString(key);
		try {
			return Boolean.valueOf(value);
		} catch (Exception e) {
			return null;
		}
	}

	public static BigDecimal getBigDecimal(String key) {
		String value = getString(key);
		try {
			return new BigDecimal(value);
		} catch (Exception e) {
			return null;
		}
	}

}
