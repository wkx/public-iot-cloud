package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.PhoneDomain;
import cn.com.flaginfo.model.dto.PhoneListInputDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/9.
 * @author wkx
 */
@Mapper
public interface PhoneMapper {

    List<PhoneDomain> findPage(PhoneListInputDTO dto);

    int findCount(PhoneListInputDTO dto);

    PhoneDomain findByPhone(@Param("iccId") String iccId, @Param("phone") String phone);

    void save(PhoneDomain domain);

    void delete(@Param("id") int id);

    void update(PhoneDomain domain);

    List<Map<String,Object>> findAll(PhoneDomain domain);

}
