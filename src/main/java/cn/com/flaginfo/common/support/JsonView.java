//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package cn.com.flaginfo.common.support;

import java.util.HashMap;
import java.util.Map;

public class JsonView extends HashMap<String, Object> {
    public static final String RETURN_SUCC = "200";
    public static final String RETURN_FAIL = "499";
    public static final String RETURN_DATA_ERROR = "502";
    public static final String RETURN_DOUBLE_ERROR = "503";
    public static final String RETURN_MAX_ERROR = "508";
    public static final String RETURN_NULL_ERROR = "500";
    public static final String RETURN_EXE_FAIL = "499";
    public static final Map<String, String> RETURN_MSG_MAP = new HashMap<>();

    public JsonView() {
        this.setSuccess();
    }

    public JsonView(String code, String msg) {
        this.put(code, msg);
    }

    public void addAttribute(String key, Object value) {
        this.put(key, value);
    }

    public Object getArttribute(String key) {
        return this.get(key);
    }

    public void setSuccess() {
        this.put("returnCode", "200");
    }

    public void setFail() {
        this.put("returnCode", "499");
    }

    public void setFail(String failCode) {
        if(this.get("returnMsg") == null) {
            this.setReturnMsg((String)RETURN_MSG_MAP.get(failCode));
        }

        this.put("returnCode", failCode);
    }

    public void setReturnMsg(String msg) {
        this.put("returnMsg", msg);
    }

    public String getReturnCode() {
        return (String)this.get("returnCode");
    }

    public void setReturnCode(String returnCode) {
        if(this.get("returnMsg") == null) {
            this.setReturnMsg((String)RETURN_MSG_MAP.get(returnCode));
        }

        this.put("returnCode", returnCode);
    }

    public String getReturnMsg() {
        return (String)this.get("returnMsg");
    }


    public static JsonView dataErrorView(String message) {
        return new JsonView("502", message);
    }


    public static JsonView getDataErrorView(String message) {
        JsonView jsonView = new JsonView();
        jsonView.setReturnCode("502");
        jsonView.setReturnMsg(message);
        return jsonView;
    }

    public static JsonView getNullErrorView(String message) {
        JsonView jsonView = new JsonView();
        jsonView.setReturnMsg(message);
        jsonView.setReturnCode("500");
        return jsonView;
    }

    public static JsonView getDoubleErrorView(String message) {
        JsonView jsonView = new JsonView();
        jsonView.setReturnCode("503");
        jsonView.setReturnMsg(message);
        return jsonView;
    }

    public static JsonView getExeFailView(String message) {
        JsonView jsonView = new JsonView();
        jsonView.setReturnCode("499");
        jsonView.setReturnMsg(message);
        return jsonView;
    }

    public static JsonView getSuccView() {
        return getSuccView((String)null);
    }

    public static JsonView getSuccView(String message) {
        JsonView jsonView = new JsonView();
        jsonView.setReturnCode("200");
        jsonView.setReturnMsg(message == null?"成功":message);
        return jsonView;
    }

    public static JsonView getMapView(Map map) {
        JsonView jsonView = new JsonView();
        if(map != null) {
            jsonView.putAll(map);
        }

        return jsonView;
    }

    static {
        RETURN_MSG_MAP.put("200", "操作成功");
        RETURN_MSG_MAP.put("502", "数据异常");
        RETURN_MSG_MAP.put("503", "数据重复");
        RETURN_MSG_MAP.put("500", "数据字段有空");
        RETURN_MSG_MAP.put("499", "操作异常");
    }
}
