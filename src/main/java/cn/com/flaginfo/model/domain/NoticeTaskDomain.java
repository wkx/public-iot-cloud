package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class NoticeTaskDomain {

    private String id;

    private String spId;

    private String accountId;

    private String iccId;

    private String content;

    private Integer noticeNum;

    private String taskId;

    private Date createTime;

    private Integer type;

}
