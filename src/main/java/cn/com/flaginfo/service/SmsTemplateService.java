package cn.com.flaginfo.service;

import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.common.util.Constant;
import cn.com.flaginfo.common.util.InsideApi;
import cn.com.flaginfo.mapper.SmsTemplateMapper;
import cn.com.flaginfo.model.domain.SmsTemplateDomain;
import cn.com.flaginfo.model.dto.SmsTemplateDelDTO;
import cn.com.flaginfo.model.dto.SmsTemplateListDTO;
import cn.com.flaginfo.model.dto.SmsTemplateUpdateDTO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Service
public class SmsTemplateService {

    @Autowired
    private SmsTemplateMapper smsTemplateMapper;

    private static final Logger LOGGER= LoggerFactory.getLogger(SmsTemplateService.class);

    private JSONObject getPage(int pageNo,int pageLimit){
        JSONObject json=new JSONObject();
        json.put("curPage",pageNo);
        json.put("pageLimit",pageLimit);
        return json;
    }

    private String getInstruct(String id,List<Map<String,Object>> instructList){
        for(Map<String,Object> map:instructList){
            if(id.equals(String.valueOf(map.get("templateId"))))
                return String.valueOf(map.get("instruct"));
        }
        return null;
    }

    private JSONObject getAllTemplate(String spId,String text,String status){
        JSONObject requestParam=new JSONObject();
        requestParam.put("pageInfo",getPage(1,1));
        requestParam.put("spId",spId);
        String [] statusArray={"0","1","2"};
        if(status!=null)
            statusArray=new String[]{status};
        requestParam.put("status",statusArray);
        requestParam.put("text",text);
        String requestParamStr=requestParam.toJSONString();
        LOGGER.info("生成请求参数：{}",requestParamStr);
        JSONObject result=InsideApi.postForJson("sms_tpl_list",requestParamStr);
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版查询失败");
        int dataCount=result.getIntValue("dataCount");
        requestParam.remove("pageInfo");
        requestParam.put("pageInfo",getPage(1,dataCount));
        result=InsideApi.postForJson("sms_tpl_list",requestParam.toJSONString());
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版查询失败");
        return result;
    }

    private Map<String,Object> list(String spId,String text,String status,int pageNo,int pageLimit){
        JSONObject requestParam=new JSONObject();
        requestParam.put("pageInfo",getPage(pageNo,pageLimit));
        requestParam.put("spId",spId);
        String [] statusArray={"0","1","2"};
        if(status!=null)
            statusArray=new String[]{status};
        requestParam.put("status",statusArray);
        requestParam.put("text",text);
        String requestParamStr=requestParam.toJSONString();
        LOGGER.info("生成请求参数：{}",requestParamStr);
        JSONObject result=InsideApi.postForJson("sms_tpl_list",requestParamStr);
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版查询失败");
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("dataCount",result.getIntValue("dataCount"));
        resultMap.put("list",result.getJSONArray("list"));
        return resultMap;
    }

    private Map<String,Object> list(String spId,String text,String status,String instruct,int pageNo,int pageLimit){
        JSONObject result=getAllTemplate(spId,text,status);
        JSONArray array=result.getJSONArray("list");
        List<JSONObject> finalList=new ArrayList<>();
        List<Map<String,Object>> instructList=smsTemplateMapper.findLikeInstruct(spId,instruct);
        for(Object obj:array){
            JSONObject json= (JSONObject) obj;
            String id=json.getString("id");
            String instructLike=getInstruct(id,instructList);
            if(instructLike==null)continue;
            json.put("instruct",instructLike);
            finalList.add(json);
        }
        int begin=(pageNo-1)*pageLimit;
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("dataCount",finalList.size());
        resultMap.put("list",finalList.subList(begin,begin+pageLimit));
        return resultMap;
    }

    public Map<String,Object> list(SmsTemplateListDTO dto,String spId){
        if(dto.getInstruct()==null)
            return list(spId,dto.getContent(),dto.getStatus(),dto.getPageNo(),dto.getPageLimit());
        return list(spId,dto.getContent(),dto.getStatus(),dto.getInstruct(),dto.getPageNo(),dto.getPageLimit());
    }

    public void add(SmsTemplateDomain domain){
        SmsTemplateDomain resultDomain=smsTemplateMapper.findByInstruct(domain.getSpId(),domain.getInstruct());
        if(resultDomain!=null)
            throw new IotException("499","指令已经存在");
        JSONObject requestParam=new JSONObject();
        requestParam.put("text",domain.getContent());
        requestParam.put("source","1");
        requestParam.put("status","1");
        requestParam.put("spId",domain.getSpId());
        LOGGER.info("新增模版信息");
        JSONObject result=InsideApi.postForJson("sms_tpl_add",requestParam.toJSONString());
        LOGGER.info("新增模版获取的返回结果：{}",result.toJSONString());
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版新增失败");
        String templateId=result.getString("id");
        domain.setTemplateId(templateId);
        domain.setCreateTime(new Date());
        smsTemplateMapper.save(domain);
    }

    @Transactional
    public void update(SmsTemplateUpdateDTO dto){
        smsTemplateMapper.update(dto);
        JSONObject requestParam=new JSONObject();
        requestParam.put("id",dto.getTemplateId());
        requestParam.put("text",dto.getContent());
        JSONObject result=InsideApi.postForJson("sms_tpl_modify",requestParam.toJSONString());
        LOGGER.info("修改模版获取的返回结果：{}",result.toJSONString());
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版信息修改失败");
    }

    public void delete(SmsTemplateDelDTO dto){
        JSONArray idArray=JSONArray.parseArray(dto.getIds());
        List<String> list=new ArrayList<>();
        for(Object obj:idArray)
            list.add(((JSONObject)obj).getString("id"));
        JSONObject requestParam=new JSONObject();
        requestParam.put("id", list);
        requestParam.put("remark","删除模板" );
        requestParam.put("operateType", "2");
        JSONObject result=InsideApi.postForJson("sms_tpl_status_update",requestParam.toJSONString());
        LOGGER.info("修改模版获取的返回结果：{}",result.toJSONString());
        if(!Constant.SUCCESS_CODE.equals(result.getString("returnCode")))
            throw new IotException("499","模版信息删除失败");
    }

}
