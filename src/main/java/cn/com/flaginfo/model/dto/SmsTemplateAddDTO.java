package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SmsTemplateAddDTO {

    @NotNull(message = "模版内容不可为空")
    private String content;

    private String order;

}
