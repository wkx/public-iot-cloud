package cn.com.flaginfo.controller;

import cn.com.flaginfo.Task.ImportTask;
import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.common.util.FileUtils;
import cn.com.flaginfo.common.util.Tools;
import cn.com.flaginfo.model.domain.AccountDomain;
import cn.com.flaginfo.model.domain.CardDomain;
import cn.com.flaginfo.model.domain.CardGroupDomain;
import cn.com.flaginfo.model.domain.PhoneDomain;
import cn.com.flaginfo.model.dto.*;
import cn.com.flaginfo.model.pojo.SendToIccCard;
import cn.com.flaginfo.service.CardGroupService;
import cn.com.flaginfo.service.CardService;
import cn.com.flaginfo.service.ImportService;
import cn.com.flaginfo.service.PhoneService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@RequestMapping("/card")
@RestController
public class CardController extends BaseController{

    private final CardService cardService;

    private final CardGroupService cardGroupService;

    private final PhoneService phoneService;

    @Autowired
    private final ImportService importService;

    @Autowired
    private ImportTask importTask;

    private static final Logger LOGGER= LoggerFactory.getLogger(CardController.class);

    @Autowired
    public CardController(CardService cardService, CardGroupService cardGroupService,
                          PhoneService phoneService, ImportService importService) {
        this.cardService = cardService;
        this.cardGroupService = cardGroupService;
        this.phoneService = phoneService;
        this.importService = importService;
    }

    @RequestMapping("/synchronize")
    public JsonView synchronize(HttpSession session){
        String spId=SessionHolder.getSpId(session);
        JsonView jsonView= JsonView.getSuccView();
        cardService.synchronizeCard(spId,SessionHolder.getJasperClient(session));
        return jsonView;
    }

    @RequestMapping("/list")
    public JsonView list(@Validated CardListInputDTO dto, BindingResult bindingResult, HttpSession session){
        checkDTOParam(bindingResult);
        LOGGER.info("请求参数：{}", JSONObject.toJSONString(dto));
        String spId=SessionHolder.getSpId(session);
        JsonView jsonView=JsonView.getSuccView();
        dto.setSpId(spId);
        jsonView.addAttribute("data",cardService.list(dto));
        return jsonView;
    }

    @RequestMapping("/detail")
    public JsonView detail(@RequestParam("simId")String simId, HttpSession session){
        LOGGER.info("请求参数：{}",simId);
        JsonView jsonView=new JsonView();
        Map<String,Object> data=cardService.cardDetail(SessionHolder.getJasperClient(session),simId);
        jsonView.addAttribute("data",data);
        jsonView.setSuccess();
        return jsonView;
    }

    @RequestMapping("/update")
    public JsonView modify(@Validated CardUpdateInputDTO dto,BindingResult bindingResult,HttpSession session){
        checkDTOParam(bindingResult);
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        LOGGER.info("请求参数：{}",JSONObject.toJSONString(dto));
        CardDomain domain=new CardDomain();
        BeanUtils.copyProperties(dto,domain);
        cardService.updateCard(domain);
        return JsonView.getSuccView();
    }

    @RequestMapping("/active")
    public JsonView active(@Validated CardActiveInputDTO dto,BindingResult bindingResult,HttpSession session){
        checkDTOParam(bindingResult);
        HttpClient client=SessionHolder.getJasperClient(session);
        cardService.activeCard(client,dto);
        return JsonView.getSuccView();
    }

    @RequestMapping("/upwardMessage")
    public JsonView upwardMessage(@Validated CardUpwardMessageDTO dto,BindingResult bindingResult, HttpSession session){
        checkDTOParam(bindingResult);
        AccountDomain domain=SessionHolder.getAccount(session);
        SendToIccCard sendToIccCard=new SendToIccCard();
        BeanUtils.copyProperties(dto,sendToIccCard);
        BeanUtils.copyProperties(domain,sendToIccCard);
        cardService.upwardMessage(sendToIccCard);
        return JsonView.getSuccView();
    }

    @RequestMapping("/group/list")
    public JsonView groupList(HttpSession session){
        String spId=SessionHolder.getSpId(session);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",cardGroupService.list(spId));
        return jsonView;
    }

    @RequestMapping("/group/save")
    public JsonView groupSave(@RequestParam("name") String name,HttpSession session){
        LOGGER.info("请求参数：{}",name);
        String spId=SessionHolder.getSpId(session);
        cardGroupService.save(name,spId);
        return JsonView.getSuccView();
    }

    @RequestMapping("/group/update")
    public JsonView groupUpdate(@Validated CardGroupUpdateDTO dto,BindingResult result,HttpSession session){
        checkDTOParam(result);
        String spId=SessionHolder.getSpId(session);
        CardGroupDomain domain=new CardGroupDomain();
        BeanUtils.copyProperties(dto,domain);
        domain.setSpId(spId);
        cardGroupService.update(domain);
        return JsonView.getSuccView();
    }

    @RequestMapping("/group/delete")
    public JsonView groupDelete(@RequestParam("id")Integer id){
        cardGroupService.delete(id);
        return JsonView.getSuccView();
    }

    @RequestMapping("/phone/list")
    public JsonView phoneList(@Validated PhoneListInputDTO dto,BindingResult bResult){
        checkDTOParam(bResult);
        LOGGER.info("获取的参数：{}",JSONObject.toJSONString(dto));
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",phoneService.list(dto));
        return jsonView;
    }

    @RequestMapping("/phone/save")
    public JsonView phoneSave(@Validated PhoneSaveDTO dto,BindingResult bResult){
        checkDTOParam(bResult);
        LOGGER.info("获取的参数：{}",JSONObject.toJSONString(dto));
        PhoneDomain domain=new PhoneDomain();
        BeanUtils.copyProperties(dto,domain);
        phoneService.save(domain);
        return JsonView.getSuccView();
    }

    @RequestMapping("/phone/update")
    public JsonView phoneUpdate(@Validated PhoneUpdateDTO dto,BindingResult bindingResult){
        checkDTOParam(bindingResult);
        LOGGER.info("请求参数：{}",JSONObject.toJSONString(dto));
        PhoneDomain domain=new PhoneDomain();
        BeanUtils.copyProperties(dto, domain);
        phoneService.update(domain);
        return JsonView.getSuccView();
    }

    @RequestMapping("/phone/delete")
    public JsonView phoneDelete(@RequestParam("ids") String ids){
        LOGGER.info("请求参数：{}",ids);
        JSONObject object=JSONObject.parseObject(ids);
        JSONArray idArray=object.getJSONArray("ids");
        phoneService.delete(idArray);
        return JsonView.getSuccView();
    }

    @RequestMapping("/map/list")
    public JsonView mapList(@Validated MapListDTO dto,BindingResult result,HttpSession session){
        checkDTOParam(result);
        LOGGER.info("请求参数：{}",JSONObject.toJSONString(dto));
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        List<Map<String,Object>> list=cardService.mapList(dto);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",list);
        return jsonView;
    }

    @RequestMapping("/import-card")
    public JsonView importCard(HttpServletRequest request){
        HttpSession session=request.getSession();
        String spId=SessionHolder.getSpId(session);
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");
        System.out.println(file.getName());
        try {
            importTask.importCard(file.getInputStream(),spId);
        } catch (IOException e) {
            return JsonView.dataErrorView("文件解析失败");
        }
        return JsonView.getSuccView();
    }

    @RequestMapping("/import-phone/{iccId}")
    public JsonView importPhone(HttpServletRequest request, @PathVariable String iccId){
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");
        String spId=SessionHolder.getSpId(request.getSession());
        try {
            importTask.importMdn(file.getInputStream(),spId,iccId);
        } catch (IOException e) {
            return JsonView.dataErrorView("文件解析失败");
        }
        return JsonView.getSuccView();
    }

    @RequestMapping("/import/error/list")
    public JsonView errorList(@Validated ErrorFileListInputDTO dto,BindingResult bindingResult,HttpSession session){
        checkDTOParam(bindingResult);
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",importService.errorFileList(dto));
        return jsonView;
    }

    @RequestMapping("/exportIccCard")
    public void exportIccCard(CardExportDTO dto, HttpSession session, HttpServletResponse response){
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        CardDomain cardDomain=new CardDomain();
        BeanUtils.copyProperties(dto,cardDomain);
        List<Map<String,Object>> list=cardService.exportIccId(cardDomain);
        JSONArray jsonArray=JSONArray.parseArray(JSONObject.toJSONString(list));
        String[] headers=new String[6];
        String[] columns=new String[6];
        headers[0]="IccId号(必填)";columns[0]="iccId";
        headers[1]="卡号说明";columns[1]="remark";
        headers[2]="关键字";columns[2]="keyWord";
        headers[3]="分组";columns[3]="groupName";
        headers[4]="手机号(必填)";columns[4]="phone";
        headers[5]="姓名(必填)";columns[5]="username";
        Tools.ExportExcel(jsonArray,headers,columns,response);
    }

    @RequestMapping("/exportPhone")
    public void exportPhone(PhoneExportDTO dto,BindingResult bindingResult,HttpServletResponse response){
        checkDTOParam(bindingResult);
        PhoneDomain phoneDomain=new PhoneDomain();
        BeanUtils.copyProperties(dto,phoneDomain);
        List<Map<String,Object>> list=phoneService.findAll(phoneDomain);
        JSONArray jsonArray=JSONArray.parseArray(JSONObject.toJSONString(list));
        String[] headers=new String[4];
        String[] columns=new String[4];
        headers[0]="手机号(必填)";columns[0]="phone";
        headers[1]="机主姓名";columns[1]="username";
        headers[2]="身份证";columns[2]="identityId";
        headers[3]="家庭住址";columns[3]="address";
        Tools.ExportExcel(jsonArray,headers,columns,response);
    }

    @RequestMapping("/downLoadFile")
    public void downLoadFile(@RequestParam("mediaId")String mediaId,@RequestParam("filename")String filename,
                             HttpServletResponse response){
        LOGGER.info("进行文件下载");
        InputStream in=FileUtils.downLoadFile(mediaId);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("content-disposition", "attachment;filename=" +filename);
        try {
            BufferedOutputStream out=new BufferedOutputStream(response.getOutputStream());
            byte [] buffer=new byte[1024];
            int length;
            while ((length=in.read(buffer))!=-1){
                out.write(buffer,0,length);
                out.flush();
            }
            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
