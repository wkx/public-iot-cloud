package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.common.util.Tools;
import cn.com.flaginfo.model.dto.RuleCreateDTO;
import cn.com.flaginfo.model.dto.RuleListDTO;
import cn.com.flaginfo.model.dto.RuleUpdateDTO;
import cn.com.flaginfo.service.RuleService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@RequestMapping("/rule")
@RestController
public class RuleController extends BaseController{

    private final RuleService ruleService;

    private static final Logger LOGGER= LoggerFactory.getLogger(RuleController.class);

    @Autowired
    public RuleController(RuleService messageService) {
        this.ruleService = messageService;
    }

    @RequestMapping("/list")
    public JsonView list(@Validated RuleListDTO dto, BindingResult bResult, HttpSession session){
        checkDTOParam(bResult);
        LOGGER.info("获取的请求参数：{}",JSONObject.toJSONString(dto));
        HttpClient client= SessionHolder.getJasperClient(session);
        String result=ruleService.ruleList(client,dto);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data", JSONObject.parseObject(result));
        return jsonView;
    }

    @RequestMapping("/enable")
    public JsonView enable(@RequestParam("id") String id,HttpSession session){
        LOGGER.info("获取的请求参数：{}",id);
        HttpClient client= SessionHolder.getJasperClient(session);
        ruleService.enableRule(client,id);
        return JsonView.getSuccView();
    }

    @RequestMapping("/disable")
    public JsonView disable(@RequestParam("id") String id,HttpSession session){
        LOGGER.info("获取的请求参数：{}",id);
        HttpClient client= SessionHolder.getJasperClient(session);
        ruleService.disableRule(client,id);
        return JsonView.getSuccView();
    }

    @RequestMapping("/delete")
    public JsonView delete(@RequestParam("id")String id,HttpSession session){
        LOGGER.info("获取的请求参数：{}",id);
        HttpClient client= SessionHolder.getJasperClient(session);
        ruleService.deleteRule(client,id);
        return JsonView.getSuccView();
    }

    @RequestMapping("/create")
    public JsonView create(@Validated RuleCreateDTO dto, BindingResult bResult,HttpSession session){
        checkDTOParam(bResult);
        HttpClient client=SessionHolder.getJasperClient(session);
        int accId=SessionHolder.getJasperAccountInfo(session).getIntValue("acctId");
        ruleService.createRule(client,dto.getCode(),dto.getCallbackUrl(), dto.getDescription(),accId);
        return JsonView.getSuccView();
    }

    @RequestMapping("/update")
    public JsonView update(@Validated RuleUpdateDTO dto,BindingResult bResult, HttpSession session){
        checkDTOParam(bResult);
        HttpClient client=SessionHolder.getJasperClient(session);
        ruleService.updateRule(client,dto.getRuleId(), dto.getCode(),dto.getCallbackUrl(),
                dto.getDescription(),dto.getPreJson());
        return JsonView.getSuccView();
    }

    @RequestMapping("/exportRule")
    public void export(@Validated RuleListDTO dto, BindingResult bResult, HttpSession session,
                       HttpServletResponse response){
        checkDTOParam(bResult);
        String headers[]=new String[5];
        String columns[]=new String[5];
        HttpClient client=SessionHolder.getJasperClient(session);
        JSONArray array=ruleService.exportRuList(dto,client,response);
        Tools.ExportExcel(array,headers,columns,response);
    }

}
