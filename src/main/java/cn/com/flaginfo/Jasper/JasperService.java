package cn.com.flaginfo.Jasper;

import cn.com.flaginfo.model.pojo.SendToIccCard;
import com.sun.xml.wss.ProcessingContext;
import com.sun.xml.wss.XWSSProcessor;
import com.sun.xml.wss.XWSSProcessorFactory;
import com.sun.xml.wss.impl.callback.PasswordCallback;
import com.sun.xml.wss.impl.callback.UsernameCallback;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wkx on 2017/3/10.
 * @author wkx
 */
@Component
public  class JasperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JasperService.class);
    private static MessageFactory messageFactory;
    private static XWSSProcessorFactory processorFactory;
    private static final String NAMESPACE_URI = "http://api.jasperwireless.com/ws/schema";
    private static final String PREFIX = "sch";

    static {
        try {
            messageFactory = MessageFactory.newInstance();
            processorFactory = XWSSProcessorFactory.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("初始化异常");
        }
    }

    SOAPMessage generateMessage(String rootKey, String soapAction, Map<String, Object> paramMap) {
        try {
            SOAPMessage message = messageFactory.createMessage();
            message.getMimeHeaders().addHeader("SOAPAction", soapAction);
            SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
            Name requestName = envelope.createName(rootKey, PREFIX, NAMESPACE_URI);
            SOAPBodyElement rootElement = message.getSOAPBody().addBodyElement(requestName);
            for (String $key : paramMap.keySet()) {
                Object obj = paramMap.get($key);
                Name $name = envelope.createName($key, PREFIX, NAMESPACE_URI);
                rootElement.addChildElement($name).setValue(String.valueOf(obj));
            }
            return message;
        } catch (SOAPException e) {
            LOGGER.error("生成SOAPMessage错误", e);
        }
        return null;
    }

    private SOAPMessage secureMessage(SOAPMessage message, final String username, final String password) throws IOException {
        LOGGER.info("生成安全消息，将来统一放入父类");
        InputStream policyStream = null;
        try {
            policyStream = getClass().getClassLoader().getResourceAsStream("SecurityPolicy.xml");
            XWSSProcessor processor  = processorFactory.createProcessorForSecurityConfiguration(policyStream,
                    getCallBackHandler(username,password));
            ProcessingContext context = processor.createProcessingContext(message);
            System.out.println("测试");
            return processor.secureOutboundMessage(context);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
            return null;
        } finally {
            if (policyStream != null) {
                policyStream.close();
            }
        }
    }

    private CallbackHandler getCallBackHandler(final String username, final String password){
        return  new CallbackHandler() {
            public void handle(Callback[] callbacks) throws UnsupportedCallbackException {
                for (Callback callback1 : callbacks) {
                    if (callback1 instanceof UsernameCallback) {
                        ((UsernameCallback) callback1).setUsername(username);
                    } else if (callback1 instanceof PasswordCallback) {
                        ((PasswordCallback) callback1).setPassword(password);
                    } else {
                        throw new UnsupportedCallbackException(callback1);
                    }
                }
            }
        };
    }

    private String callWebServiceWithHttp(SOAPMessage request, String url, String username, String password){
        HttpClient client= HttpClients.custom().build();
        HttpPost post=new HttpPost(url);
        String soapAction[]=request.getMimeHeaders().getHeader("SOAPAction");
        try {
            request=this.secureMessage(request,username,password);
            ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
            if(request!=null)request.writeTo(outputStream);
            else throw new RuntimeException("安全消息生成失败");
            Document doc = request.getSOAPPart().getEnvelope().getOwnerDocument();
            StringWriter output = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform( new DOMSource(doc), new StreamResult(output));
            System.out.println("请求信息"+output.toString());
            post.addHeader(new BasicHeader("Content-Type","text/xml; charset=UTF-8"));
            post.addHeader(new BasicHeader("SOAPAction",soapAction[0]));
            post.setEntity(new ByteArrayEntity(outputStream.toByteArray()));
            HttpResponse response=client.execute(post);
            if(response.getStatusLine().getStatusCode()== HttpStatus.SC_OK){
                String resultStr= EntityUtils.toString(response.getEntity());
                LOGGER.info("获取的结果:"+resultStr);
                return resultStr;
            }else{
                LOGGER.info("请求访问错误状态码："+response.getStatusLine().getStatusCode());
                LOGGER.info("获取的结果:"+EntityUtils.toString(response.getEntity()));
                throw new RuntimeException("物联网卡短信发送失败");
            }
        } catch (IOException | SOAPException | TransformerException e) {
            LOGGER.error("请求发生异常",e);
            throw new RuntimeException("物联网卡短信发送失败");
        }
    }

    public void sendSms2Device(SendToIccCard sendToIccCard) {
        LOGGER.info("向物联网卡发送短信");
        long beginTime = System.currentTimeMillis();
        Map<String,Object> param=new LinkedHashMap<>();
        param.put("messageId",System.currentTimeMillis());
        param.put("version","1.0");
        param.put("licenseKey",sendToIccCard.getLicenseKey());
        param.put("sentToIccid",sendToIccCard.getIccId());
        param.put("messageText",sendToIccCard.getContent());
        SOAPMessage message=this.generateMessage("SendSMSRequest","http://api.jasperwireless.com/ws/service/sms/SendSMS",param);
        String url =sendToIccCard.getApiUrl() + "/ws/service/Sms";
        this.callWebServiceWithHttp(message,url,sendToIccCard.getUsername(),sendToIccCard.getPassword());
        LOGGER.info("向设备发送短信----->>end-->>cost " + (System.currentTimeMillis() - beginTime) +" ms-----------");
    }
}
