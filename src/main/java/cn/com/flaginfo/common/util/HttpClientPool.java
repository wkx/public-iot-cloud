package cn.com.flaginfo.common.util;


import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.LayeredConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 *  http连接池
 */
public class HttpClientPool {

    private static final Logger LOGGER= LoggerFactory.getLogger(HttpClientPool.class);

    private static PoolingHttpClientConnectionManager connectionManager;

    private final static int MAX_TOTAL_CONNECTIONS = 800;

    private final static int MAX_ROUTE_CONNECTIONS = 400;

    static {
        LayeredConnectionSocketFactory ssl;
        try {
            ssl = new SSLConnectionSocketFactory(SSLContext.getDefault());
            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
                    .register("https", ssl)
                    .register("http", new PlainConnectionSocketFactory())
                    .build();
            connectionManager=new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            connectionManager.setMaxTotal(MAX_TOTAL_CONNECTIONS);
            connectionManager.setDefaultMaxPerRoute(MAX_ROUTE_CONNECTIONS);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error(e.getMessage(),e);
        }
    }

    public static HttpClient getHttpClient(CookieStore cookieStore){
        HttpClientBuilder builder=HttpClients.custom().setDefaultCookieStore(cookieStore);
        if(connectionManager!=null)
            builder.setConnectionManager(connectionManager);
        return builder.build();
    }


}
