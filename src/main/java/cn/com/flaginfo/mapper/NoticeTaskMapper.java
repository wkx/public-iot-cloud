package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.NoticeTaskDomain;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Mapper
public interface NoticeTaskMapper {

    void save(NoticeTaskDomain domain);
}
