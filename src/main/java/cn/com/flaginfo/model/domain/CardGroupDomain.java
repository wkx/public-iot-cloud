package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardGroupDomain {

    private int id;

    private String spId;

    private String name;

    private Date createTime;

}
