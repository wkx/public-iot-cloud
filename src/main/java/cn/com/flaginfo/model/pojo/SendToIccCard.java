package cn.com.flaginfo.model.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SendToIccCard {

    private String iccId;

    private String content;

    private String username;

    private String password;

    private String apiUrl;

    private String licenseKey;
}
