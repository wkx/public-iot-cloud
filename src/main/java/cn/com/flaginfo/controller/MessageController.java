package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.common.util.RedisFacade;
import cn.com.flaginfo.model.dto.CmcSmsReplyDTO;
import cn.com.flaginfo.model.pojo.CmcSmsReply;
import cn.com.flaginfo.service.MessageService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@RestController
@RequestMapping("/message")
public class MessageController extends BaseController{

    private final MessageService messageService;

    private static final String PREFIX = "IOT_RECEIVE_";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
    private static final String apiSecretKey = "flaginfo";

    private static final Logger LOGGER= LoggerFactory.getLogger(MessageController.class);

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping("/sms/reply")
    public JsonView forwardReply(@Validated CmcSmsReplyDTO dto, BindingResult result, HttpSession session){
        checkDTOParam(result);
        String oriContent = dto.getText();
        String keyWord = oriContent.split("#")[0];
        String content = oriContent.substring(keyWord.length() + 1);
        CmcSmsReply reply=new CmcSmsReply();
        BeanUtils.copyProperties(dto,reply);
        reply.setContent(content).setKeyWord(keyWord);
        messageService.forwardReply(reply, SessionHolder.getAccount(session));
        return JsonView.getSuccView();
    }

    @RequestMapping("receiver/{spId}")
    public JsonView receiver(HttpServletRequest request, @PathVariable String spId) {
        long beginTime = System.currentTimeMillis();
        Map<String, Object> reqMap = this.bindParamToMap(request);
        LOGGER.info("jasper平台所有数据" ,JSONObject.toJSONString(reqMap));
        if (!this.isValidSignature((String) reqMap.get("timestamp"), (String) reqMap.get("signature")))
            LOGGER.info("-------------签名验证失败！！！------v1.0版本暂不强制签名！----");
        LOGGER.info("-----------------签名验证通过-------------");
        String eventId = request.getParameter("eventId");
        LOGGER.info("eventId" + eventId);
        LOGGER.info("receiver detail spId=" + spId);
        String xmlStr = request.getParameter("data");
        LOGGER.info("receiver detail *****接收到的回调消息：{}" , xmlStr);
        String redisKey = PREFIX + spId + "_" + eventId;
        if (this.isDuplicate(redisKey))
            return JsonView.getSuccView();
        Document document;
        try {
            document = DocumentHelper.parseText(xmlStr);
        } catch (DocumentException e) {
            LOGGER.error(e.getMessage(),e);
            throw new RuntimeException("文档解析失败");
        }
        Element root = document.getRootElement();
        Element iccIdEle = root.element("iccid");
        Element mcEle = root.element("messageContent");
        String messageContent = ((String) mcEle.getData());
        Map<String, Object> map = new HashMap<>();
        map.put("content", messageContent);    // 短信内容
        map.put("iccId", iccIdEle.getData());
        map.put("accountId", spId);
        messageService.sendNoticeSms(map);
        LOGGER.info("receiver 处理耗时---->> {}",(System.currentTimeMillis() - beginTime));
        return JsonView.getSuccView();
    }

    private Map<String, Object> bindParamToMap(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Enumeration<?> enumeration = request.getParameterNames();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            map.put(key, request.getParameter(key));
        }
        return map;
    }

    private boolean isValidSignature(String timestamp, String signature) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(apiSecretKey.getBytes(), HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(keySpec);
            String expectedSignature = new String(Base64.encodeBase64(mac.doFinal(timestamp.getBytes())));
            if (expectedSignature.equals(signature)) return true;
            LOGGER.info("Invalid signature: {} does not match expected signature: {}",signature,expectedSignature);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(),e);
        }
        return false;
    }

    private synchronized boolean isDuplicate(String redisKey) {
        if (RedisFacade.get(redisKey) != null) {
            LOGGER.warn("重复消息，skip!--->,{}" ,redisKey);
            return true;
        } else {
            LOGGER.info("{} into redis!",redisKey);
            RedisFacade.set(redisKey, "1", 30, TimeUnit.MINUTES);
            return false;
        }
    }

}
