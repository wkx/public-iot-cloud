package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.CityDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx
 */
@Mapper
public interface CityMapper {

    List<Map<String,Object>> findAll(CityDomain domain);

}
