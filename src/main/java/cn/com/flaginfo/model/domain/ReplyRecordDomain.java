package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class ReplyRecordDomain {

    private int id;

    private String spId;

    private String iccId;

    private String content;

    private String msiSdn;

    private String remark;

    private String phone;

    private String username;

    private Date replyTime;

    private boolean status;

    private Date createTime;

}
