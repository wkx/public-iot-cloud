package cn.com.flaginfo.service;

import cn.com.flaginfo.Jasper.JasperClient;
import cn.com.flaginfo.mapper.RatePlanMapper;
import cn.com.flaginfo.model.domain.RatePlanDomain;
import cn.com.flaginfo.model.dto.RatePlanListDTO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Service
public class RatePlanService {

    @Autowired
    private RatePlanMapper ratePlanMapper;

    public void synchronize(String spId,HttpClient client){
        String result=JasperClient.ratePlaneList(client,1,2,null);
        int totalCount= JSONObject.parseObject(result).getIntValue("totalCount");
        result=JasperClient.ratePlaneList(client,1,totalCount,null);
        JSONArray data=JSONObject.parseObject(result).getJSONArray("data");
        for(Object obj:data){
            JSONObject json= (JSONObject) obj;
            RatePlanDomain domain=new RatePlanDomain();
            String ratePlanId=json.getString("ratePlanId");
            //TODO 付费类型需要调查
            domain.setPackageName(JasperClient.getPackageName(client,ratePlanId))
                    .setSpId(spId).setCharge(json.getBigDecimal("subscriptionPrice"))
                    .setRatePlanName(json.getString("name"))
                    .setVersionId(json.getString("planId"))
                    .setDataUsage(json.getIntValue("includedDataUsage"))
                    .setAcctName(json.getString("acctName"));
            ratePlanMapper.save(domain);
        }
    }

    public Map<String,Object> findPage(RatePlanListDTO dto){
        int index= (dto.getPageNo()-1)*dto.getPageLimit();
        dto.setIndex(index);
        Map<String,Object> result=new HashMap<>();
        result.put("list",ratePlanMapper.findPage(dto));
        result.put("dataCount",ratePlanMapper.findPageCount(dto));
        return result;
    }

    public List<Map<String,Object>> findAll(RatePlanListDTO dto){
        return ratePlanMapper.findAll(dto);
    }

}
