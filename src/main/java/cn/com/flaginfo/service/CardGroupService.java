package cn.com.flaginfo.service;

import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.mapper.CardGroupMapper;
import cn.com.flaginfo.model.domain.CardGroupDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx
 */
@Service
public class CardGroupService {

    private final CardGroupMapper cardGroupMapper;

    @Autowired
    public CardGroupService(CardGroupMapper cardGroupMapper) {
        this.cardGroupMapper = cardGroupMapper;
    }

    public List<Map<String,Object>> list(String spId){
        return cardGroupMapper.findAll(spId);
    }

    public void save(String name,String spId){
        Map<String,Object> map=cardGroupMapper.findByName(name,spId);
        if(map!=null&&!map.isEmpty())
            throw new IotException("499","该物联网卡分组已经存在");
        CardGroupDomain domain=new CardGroupDomain();
        domain.setSpId(spId).setName(name).setCreateTime(new Date());
        cardGroupMapper.save(domain);
    }

    public void update(CardGroupDomain domain){
        Map<String,Object> map=cardGroupMapper.findByName(domain.getName(),domain.getSpId());
        if(map==null||map.isEmpty())
            cardGroupMapper.update(domain);
        else {
            int id=Integer.parseInt(String.valueOf(map.get("id")));
            if(domain.getId()!=id)
                throw new IotException("499","分组名称已经存在");
            cardGroupMapper.update(domain);
        }
    }

    public void delete(int id){
        cardGroupMapper.delete(id);
    }

}
