package cn.com.flaginfo.common.support;

import cn.com.flaginfo.common.util.HttpClientPool;
import cn.com.flaginfo.model.domain.AccountDomain;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@SuppressWarnings("unchecked")
public class SessionHolder {

    private static final String ACCOUNT="account";

    private static final String JASPER_CLIENT="jasperClient";

    private static final String JASPER_ACCOUNT_INFO="jasperAccountInfo";

    private static final Logger LOGGER= LoggerFactory.getLogger(SessionHolder.class);

    public static String getSpId(HttpSession session){
        Map<String, Object> ssoUserMap = (Map<String, Object>) session.getAttribute("sso_user");
        Map<String,Object> spInfoMap= (Map<String, Object>) ssoUserMap.get("loginSpInfo");
        String spId=String.valueOf(spInfoMap.get("id"));
        LOGGER.info("获取企业的spId信息为：{}",spId);
        return spId;
    }

    public static boolean checkAccounr(HttpSession session){
        return session.getAttribute(ACCOUNT)!=null;
    }

    public static AccountDomain getAccount(HttpSession session){
        Object account=session.getAttribute(ACCOUNT);
        if(account instanceof AccountDomain) return (AccountDomain) account;
        else throw new IotException("499","用户未授权,或授权超时请重新登陆");
    }

    public static void setAccount(HttpSession session, AccountDomain domain){
        session.setAttribute(ACCOUNT,domain);
    }

    public static HttpClient getJasperClient(HttpSession session){
        Object cookieStore=session.getAttribute(JASPER_CLIENT);
        if(cookieStore==null||!(cookieStore instanceof CookieStore))
            throw new IotException("499","用户未授权,或授权超时请重新登陆");
        return HttpClientPool.getHttpClient((CookieStore) cookieStore);
    }

    public static void setJasperCookie(HttpSession session,CookieStore cookieStore){
        session.setAttribute(JASPER_CLIENT,cookieStore);
    }

    public static void setJasperAccountInfo(HttpSession session, JSONObject json){
        session.setAttribute(JASPER_ACCOUNT_INFO,json);
    }

    public static JSONObject getJasperAccountInfo(HttpSession session){
        Object json=session.getAttribute(JASPER_ACCOUNT_INFO);
        if(json instanceof JSONObject) return (JSONObject) json;
        else throw new IotException("499","用户未授权,或授权超时请重新登陆");
    }

}
