package cn.com.flaginfo.service;

import cn.com.flaginfo.Jasper.JasperClient;
import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.mapper.AccountMapper;
import cn.com.flaginfo.model.domain.AccountDomain;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@Service
public class AccountService {

    private final AccountMapper accountMapper;

    private static final Logger LOGGER= LoggerFactory.getLogger(AccountService.class);

    @Autowired
    public AccountService(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    public Map<String,Object> authorize(HttpSession session, String spId){
        AccountDomain domain=accountMapper.findBySpId(spId);
        Map<String,Object> result=new HashMap<>();
        LOGGER.info("jasper企业平台账号合法性检查");
        if(domain==null){
            LOGGER.info("用户没有绑定Jasper企业平台账号");
            result.put("bind",false);
            return result;
        }
        LOGGER.info("用户已经绑定Jasper企业平台账号");
        result.put("bind",true);
        if(SessionHolder.checkAccounr(session)){
            LOGGER.info("用户已经Jasper企业平台账号的模拟登陆");
            result.put("account",domain);
            return result;
        }
        LOGGER.info("用户模拟登陆,用户名：{},密码：{}",domain.getUsername(),domain.getPassword());
        CookieStore cookieStore=JasperClient.getClient(domain.getUsername(),domain.getPassword());
        LOGGER.info("模拟登陆成功,存储用户的相关cookie信息");
        SessionHolder.setJasperCookie(session,cookieStore);
        LOGGER.info("存储本地的账号信息:{}",domain);
        SessionHolder.setAccount(session,domain);
        result.put("account",domain);
        getJasperAccountInfo(session);
        return result;
    }

    @Async
    private void getJasperAccountInfo(HttpSession  session){
        LOGGER.info("获取jasper平台的企业信息");
        HttpClient client=SessionHolder.getJasperClient(session);
        String userInfo=JasperClient.getJasperUserInfo(client);
        LOGGER.info("存储用户Jasper平台的账号信息:{}",userInfo);
        SessionHolder.setJasperAccountInfo(session, JSONObject.parseObject(userInfo));
    }


    public AccountDomain bindAccount(HttpSession session, String spId, AccountDomain domain){
        AccountDomain accountDomain=accountMapper.findByUsername(domain.getUsername());
        if(accountDomain!=null)
            throw new IotException("499","此账号已经在平台上进行绑定");
        CookieStore cookieStore= JasperClient.getClient(domain.getUsername(),domain.getPassword());
        LOGGER.info("设置用户的cookieStore");
        SessionHolder.setJasperCookie(session,cookieStore);
        Map<String,Object> licenseKeyMap= JasperClient.getLicense(SessionHolder.getJasperClient(session));
        LOGGER.info("获取LicenseKeyMap:{}",licenseKeyMap);
        String apiLicenseKey=String.valueOf(licenseKeyMap.get("apiLicenseKey"));
        if(!domain.getLicenseKey().equals(apiLicenseKey))
            throw new IotException("499","License KEY错误");
        String apiHost=String.valueOf(licenseKeyMap.get("apiHost"));
        if(!domain.getApiUrl().equals(apiHost))
            throw new IotException("499","API server错误");
        domain.setSpId(spId).setCreateTime(new Date());
        accountMapper.save(domain);
        SessionHolder.setAccount(session,domain);
        return domain;
    }

}
