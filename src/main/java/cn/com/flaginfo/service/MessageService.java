package cn.com.flaginfo.service;

import cn.com.flaginfo.Jasper.JasperService;
import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.common.util.Constant;
import cn.com.flaginfo.common.util.Tools;
import cn.com.flaginfo.mapper.*;
import cn.com.flaginfo.model.domain.AccountDomain;
import cn.com.flaginfo.model.domain.NoticeTaskDomain;
import cn.com.flaginfo.model.domain.PhoneDomain;
import cn.com.flaginfo.model.domain.ReplyRecordDomain;
import cn.com.flaginfo.model.pojo.CmcSmsReply;
import cn.com.flaginfo.model.pojo.SendToIccCard;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Service
public class MessageService {

    private final CardMapper cardMapper;

    private final AccountMapper accountMapper;

    private final PhoneMapper phoneMapper;

    private final NoticeTaskMapper taskMapper;

    private final ReplyRecordMapper replyRecordMapper;

    private final JasperService jasperService;

    private static final Logger LOGGER= LoggerFactory.getLogger(MessageService.class);

    @Autowired
    public MessageService(CardMapper cardMapper, AccountMapper accountMapper, PhoneMapper phoneMapper,
                          NoticeTaskMapper taskMapper, ReplyRecordMapper replyRecordMapper,
                          JasperService jasperService) {
        this.cardMapper = cardMapper;
        this.accountMapper = accountMapper;
        this.phoneMapper = phoneMapper;
        this.taskMapper = taskMapper;
        this.replyRecordMapper = replyRecordMapper;
        this.jasperService = jasperService;
    }

    public void forwardReply(CmcSmsReply reply, AccountDomain accountDomain) {
        List<Map<String,Object>> cardList=new ArrayList<>();
        Map<String,Object> cardMap=cardMapper.findByKeyWordAndPhone(reply);
        LOGGER.info("按照关键字查询查询绑定物联网卡信息：{}",cardMap);
        if(cardMap!=null&&!cardMap.isEmpty()) cardList.add(cardMap);
        else{
            cardList=cardMapper.findByGroupNameAndPhone(reply);
            LOGGER.info("按照分组名查询查询绑定物联网卡信息：{}",cardList);
            if(cardList==null||cardList.isEmpty())
                throw new IotException("500","物联网卡不存在");
        }
        for(Map<String,Object> map:cardList){
            LOGGER.info("向物联网卡发送短信：{}",map);
            SendToIccCard sendToIccCard=new SendToIccCard();
            BeanUtils.copyProperties(accountDomain,sendToIccCard);
            sendToIccCard.setIccId(String.valueOf(map.get("iccId"))).
                    setContent(reply.getContent());
            boolean status=true;
            try {
                jasperService.sendSms2Device(sendToIccCard);
            }catch (Exception e){
                LOGGER.error(e.getMessage(),e);
                status=false;
            }
            LOGGER.info("保存用户的上行记录");
            SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
            Date replyDate=null;
            try {
                replyDate=format.parse(reply.getReplyTime());
            } catch (ParseException e) {
                LOGGER.error(e.getMessage(),e);
            }
            ReplyRecordDomain domain=new ReplyRecordDomain();
            domain.setContent(reply.getContent()).setStatus(status).setCreateTime(new Date())
                    .setReplyTime(replyDate).setSpId(String.valueOf(map.get("spId")))
                    .setIccId(String.valueOf(map.get("iccId"))).setMsiSdn(String.valueOf(map.get("msiSdn")))
                    .setRemark(String.valueOf(map.get("remark"))).setPhone(String.valueOf(map.get("phone")))
                    .setUsername(String.valueOf(map.get("username")));
            replyRecordMapper.save(domain);
        }
    }

    private String sendCmcMsg(Map<String,Object> smsParams, String spId) {
        LOGGER.info("请求平台信息,获取企业信息");
        Map<String,Object> spInfoRequest=new HashMap<>();
        spInfoRequest.put("spId",spId);
        spInfoRequest.put("spCode",null);
        JSONObject spInfoJson=Tools.postForJson("get_sp_info",spInfoRequest).getJSONObject("spInfo");
        Map<String, Object> params = new HashMap<>();
        params.put("sms", "1"); // 发送短信
        params.put("sendSource", "1"); // 发送来源：1平台发送；2接口发送
        params.put("mediaType", "text"); // 素材类型：text
        params.put("sendType", "1"); // 发送类型：1立即发送；2预约发送；3生日预约发送
        params.put("spId", spId);
        params.put("userId", spId);
        params.put("platform", spInfoJson.getString("platform"));
        params.put("signPosition", "0"); // 企业签名位置：0前置 1后置
        params.put("text", smsParams.get("content"));
        Map<String, Object> sendObjectMap = new HashMap<>();
        sendObjectMap.put("mdn", smsParams.get("mdns"));
        params.put("sendObject", sendObjectMap);
        LOGGER.info("生成的请求参数为:{}",params);
        JSONObject resultJson = Tools.postForJson("cmc_try_and_send", params);
        if (resultJson == null || !Constant.SUCCESS_CODE.equals(resultJson.get("returnCode")))
            return null;
        JSONObject smsJson=resultJson.getJSONObject("sms");
        boolean canSend = smsJson.getBoolean("canSend");
        if (!canSend) return null;
        JSONArray taskJsonArray=smsJson.getJSONArray("taskList");
        for(Object obj:taskJsonArray){
            JSONObject task= (JSONObject) obj;
            String productId = task.getString("productId");
            if("1".equals(productId))
                return task.getString("id");
        }
        return null;
    }

    @Async
    public void sendNoticeSms(Map<String,Object> params) {
        String spId=String.valueOf(params.get("spId"));
        AccountDomain accountDomain=accountMapper.findBySpId(spId);
        LOGGER.info("根据spId获取企业平台的账号信息：{}",JSONObject.toJSONString(accountDomain));
        if(accountDomain==null)
            throw new IotException("499","相关企业不存在");
        String iccId=String.valueOf(params.get("iccId"));
        LOGGER.info("获取此物联网卡下所有用户信息，并且拼接手机号信息");
        List<Map<String,Object>> phoneList=phoneMapper.findAll(new PhoneDomain().setIccId(iccId));
        if(phoneList==null||phoneList.size()>0)return;
        StringBuilder sb=new StringBuilder("");
        int length=phoneList.size();
        for (int i = 0; i < length - 1; i++)
            sb.append(String.valueOf(phoneList.get(i).get("mdn"))).append(";");
        sb.append(String.valueOf(phoneList.get(length-1).get("mdn")));
        LOGGER.info("获取此物联网卡下所有用户信息，拼接所有的手机号信息为{}",sb.toString());
        params.put("mdns",sb.toString());
        String taskId = sendCmcMsg(params, spId);
        LOGGER.info("获取短信的任务ID：{}",taskId);
        if (StringUtils.isBlank(taskId)) {
            LOGGER.info("短信发送失败：{}", JSONObject.toJSONString(params));
            return;
        }
        LOGGER.info("保存发送任务");
        NoticeTaskDomain task = new NoticeTaskDomain();
        task.setSpId(spId).setIccId(iccId).setTaskId(taskId).setType(1)
        .setContent(String.valueOf(params.get("content"))).setCreateTime(new Date())
        .setNoticeNum(phoneList.size());
        taskMapper.save(task);
    }
}
