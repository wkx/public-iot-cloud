package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class RatePlanListDTO {

    private String spId;

    private String packageName;

    private String ratePlanName;

    @Min(value = 1,message = "pageNo最小为1")
    private int pageNo;
    @Min(value = 1,message = "pageLimit最小为1")
    private int pageLimit;

    private int index;

}
