package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SmsTemplateDomain {

    private int id;

    private String spId;

    private String templateId;

    private String content;

    private String instruct;

    private Date createTime;

}
