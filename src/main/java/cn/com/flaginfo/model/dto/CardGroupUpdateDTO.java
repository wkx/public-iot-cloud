package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardGroupUpdateDTO {
    @NotNull(message = "id不可为空")
    private int id;
    @NotNull(message = "name不可为空")
    private String name;
}
