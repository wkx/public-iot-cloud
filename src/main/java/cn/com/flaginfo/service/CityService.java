package cn.com.flaginfo.service;

import cn.com.flaginfo.mapper.CityMapper;
import cn.com.flaginfo.model.domain.CityDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/9.
 * @author wkx
 */
@Service
public class CityService {

    private final CityMapper cityMapper;

    @Autowired
    public CityService(CityMapper cityMapper) {
        this.cityMapper = cityMapper;
    }

    public List<Map<String,Object>> list(CityDomain domain){
        return cityMapper.findAll(domain);
    }

    public List<Map<String,Object>> data(){
        CityDomain provinceDomain=new CityDomain();
        provinceDomain.setLevel(1);
        List<Map<String,Object>> provinceList=cityMapper.findAll(provinceDomain);
        for(Map<String,Object> provinceMap:provinceList){
            int provinceId=Integer.parseInt(String.valueOf(provinceMap.get("cityId")).substring(0,2));
            List<Map<String,Object>> cityList=cityMapper.findAll(new CityDomain().setCityId(provinceId).setLevel(2));
            for(Map<String,Object> cityMap:cityList){
                int cityId=Integer.parseInt(String.valueOf(cityMap.get("cityId")).substring(0,4));
                List<Map<String,Object>> areList=cityMapper.findAll(new CityDomain().setCityId(cityId).setLevel(3));
                cityMap.put("Children",areList);
            }
            provinceMap.put("Children",cityList);
        }
        return provinceList;
    }

}
