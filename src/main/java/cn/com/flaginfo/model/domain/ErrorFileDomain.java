package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class ErrorFileDomain {

    private int id;

    private String spId;

    private String iccId;

    private String mediaId;

    private String path;

    private String fileName;

    private int errorCount;

    private int type;

    private Date updateTime;

    private Date createTime;

}
