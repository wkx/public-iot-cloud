package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SmsTemplateUpdateDTO {

    @NotNull(message = "templateId不能为空")
    private String templateId;
    @NotNull(message = "content不能为空")
    private String content;

    private String instruct;

}
