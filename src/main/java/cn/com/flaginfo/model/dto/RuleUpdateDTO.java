package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class RuleUpdateDTO {

    @NotNull(message = "ruleId不可为空")
    private int ruleId;
    @NotNull(message = "code不可为空")
    private String code;
    @NotNull(message = "callbackUrl不可为空")
    private String callbackUrl;
    @NotNull(message = "description不可为空")
    private String description;
    @NotNull(message = "preJson不可为空")
    private String preJson;

}
