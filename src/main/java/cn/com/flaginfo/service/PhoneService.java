package cn.com.flaginfo.service;

import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.mapper.PhoneMapper;
import cn.com.flaginfo.model.domain.PhoneDomain;
import cn.com.flaginfo.model.dto.PhoneListInputDTO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/9.
 * @author wkx
 */
@Service
public class PhoneService {

    private final PhoneMapper phoneMapper;

    @Autowired
    public PhoneService(PhoneMapper phoneMapper) {
        this.phoneMapper = phoneMapper;
    }

    public Map<String,Object> list(PhoneListInputDTO dto){
        int index=(dto.getPageNo()-1)*dto.getPageLimit();
        dto.setIndex(index);
        int dataCount=phoneMapper.findCount(dto);
        Map<String,Object> result=new HashMap<>();
        result.put("dataCount",dataCount);
        result.put("list",phoneMapper.findPage(dto));
        return result;
    }

    @Transactional
    public void save(PhoneDomain domain){
        PhoneDomain phoneDomain=phoneMapper.findByPhone(domain.getIccId(),domain.getPhone());
        if(phoneDomain!=null)
            throw new IotException("499","该手机号已经绑定");
        phoneMapper.save(domain);
    }

    public void delete(JSONArray array){
        for(Object obj:array){
            JSONObject json= (JSONObject) obj;
            phoneMapper.delete(json.getInteger("id"));
        }

    }

    public void update(PhoneDomain updateDomain){
        PhoneDomain domain=phoneMapper.findByPhone(updateDomain.getIccId(),updateDomain.getPhone());
        if(domain!=null&&updateDomain.getId()!=updateDomain.getId())
            throw new IotException("499","手机号已经被绑定");
        phoneMapper.update(updateDomain);
    }

    public List<Map<String,Object>> findAll(PhoneDomain phoneDomain){
        return phoneMapper.findAll(phoneDomain);
    }

}
