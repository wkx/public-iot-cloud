package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class ErrorFileListInputDTO {

    private Integer index;

    @NotNull(message = "type不可为空")
    private Integer type;

    private String iccId;

    private String spId;

    @Min(value = 1,message = "pageNo最小为1")
    private Integer pageNo;

    @Min(value = 1,message = "pageLimit最小为1")
    private Integer pageLimit;

}
