package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.IotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
class BaseController {

    private static final Logger LOGGER= LoggerFactory.getLogger(BaseController.class);

    void checkDTOParam(BindingResult result){
        if(result.hasErrors()){
            System.out.println("获取参数错误");
            List<ObjectError> list=result.getAllErrors();
            StringBuilder sb=new StringBuilder();
            for(ObjectError error:list){
                sb.append(error.getDefaultMessage()).append(" ");
            }
            LOGGER.info("获取的错误信息为： {}",sb.toString());
            throw new IotException("499",sb.toString());
        }
    }

}
