package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.model.domain.CityDomain;
import cn.com.flaginfo.model.dto.CityListInputDTO;
import cn.com.flaginfo.service.CityService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/9.
 * @author wkx
 */
@RestController
@RequestMapping("/city")
public class CityController extends BaseController{

    private final CityService cityService;

    private static final Logger LOGGER= LoggerFactory.getLogger(CityController.class);

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @RequestMapping("/list")
    public JsonView list(@Validated CityListInputDTO dto, BindingResult bindingResult){
        checkDTOParam(bindingResult);
        LOGGER.info("获取的请求参数：{}", JSONObject.toJSONString(dto));
        CityDomain domain=new CityDomain();
        BeanUtils.copyProperties(dto,domain);
        List<Map<String,Object>> result=cityService.list(domain);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",result);
        return jsonView;
    }

    @RequestMapping("/data")
    public JsonView data(){
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",cityService.data());
        return jsonView;
    }

}
