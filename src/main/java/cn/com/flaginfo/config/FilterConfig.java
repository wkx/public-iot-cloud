package cn.com.flaginfo.config;

import cn.com.flaginfo.user.auth.sdk.SSOFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean ssoFilterRegistrationBean(){
        FilterRegistrationBean registrationBean=new FilterRegistrationBean();
        registrationBean.setName("ssoFilter");
        registrationBean.setFilter(new SSOFilter());
        List<String> urlList=new ArrayList<>();
        urlList.add("/*");
        registrationBean.addInitParameter("index_url","http://6.flaginfo.cn");
        registrationBean.addInitParameter("error_url","http://6.flaginfo.cn");
        registrationBean.addInitParameter("key_cookie_login_fla","yxt_user");
        registrationBean.addInitParameter("key_sso_session_user","loginUser");
        registrationBean.addInitParameter("exclude_paths","/user/login,/user/reg");
        registrationBean.setUrlPatterns(urlList);
        return registrationBean;
    }

//    @Bean
//    public FilterRegistrationBean devFilterRegistrationBean(){
//        FilterRegistrationBean registrationBean=new FilterRegistrationBean();
//        registrationBean.setName("devFilter");
//        registrationBean.setFilter(new DevFilter());
//        List<String> urlList=new ArrayList<>();
//        urlList.add("/*");
//        registrationBean.setUrlPatterns(urlList);
//        return registrationBean;
//    }

}
