package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class RatePlanDomain {

    private int id;

    private String spId;

    private String packageName;

    private String ratePlanName;

    private String ratePlanId;

    private String versionId;

    private BigDecimal charge;

    private String payMethod;

    private int dataUsage;

    private String acctName;

}
