package cn.com.flaginfo.Task;

import cn.com.flaginfo.common.util.FileUtils;
import cn.com.flaginfo.mapper.CardGroupMapper;
import cn.com.flaginfo.mapper.CardMapper;
import cn.com.flaginfo.mapper.ErrorFileMapper;
import cn.com.flaginfo.mapper.PhoneMapper;
import cn.com.flaginfo.model.domain.CardDomain;
import cn.com.flaginfo.model.domain.CardGroupDomain;
import cn.com.flaginfo.model.domain.ErrorFileDomain;
import cn.com.flaginfo.model.domain.PhoneDomain;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by wkx on 2017/3/10.
 *
 * @author wkx
 */
@Component
public class ImportTask {

    private final CardMapper cardMapper;

    private final PhoneMapper phoneMapper;

    @Autowired
    private final CardGroupMapper cardGroupMapper;

    private final ErrorFileMapper errorFileMapper;

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportTask.class);

    private static final String[] cardHeaders = {"ICCID号（必填）", "卡号说明", "上行关键字", "分组", "手机号(必填)", "姓名（必填）", "错误原因"};

    private static final String[] phoneHeaders = {"手机号（必填）", "姓名（必填）", "错误原因"};

    @Autowired
    public ImportTask(CardMapper cardMapper, PhoneMapper phoneMapper,
                      CardGroupMapper cardGroupMapper, ErrorFileMapper errorFileMapper) {
        this.cardMapper = cardMapper;
        this.phoneMapper = phoneMapper;
        this.cardGroupMapper = cardGroupMapper;
        this.errorFileMapper = errorFileMapper;
    }

    private void saveErrorFile(Map<Row, String> rows, String[] headers, String spId,String iccId, int type) {
        ErrorFileDomain domain = new ErrorFileDomain();
        if (rows.keySet().size() == 0) {
            domain.setIccId(iccId).setType(type).setUpdateTime(new Date())
                    .setErrorCount(rows.keySet().size()).setSpId(spId);
            errorFileMapper.save(domain);
        }
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row firstRow = sheet.createRow(0);
        LOGGER.info("生成数据头部");
        int length = headers.length;
        for (int j = 0; j < length; j++) {
            Cell cell = firstRow.createCell(j);
            cell.setCellValue(headers[j]);
        }
        int i = 1;
        for (Row $row : rows.keySet()) {
            Row row = sheet.createRow(i);
            length = $row.getLastCellNum();
            for (int j = 0; j < length; j++) {
                Cell cell = row.createCell(j);
                if ($row.getCell(j) != null) {
                    cell.setCellValue(getCellValue($row.getCell(j)));
                } else {
                    cell.setCellValue("");
                }
            }
            Cell cell = row.createCell(headers.length - 1);
            cell.setCellValue(rows.get($row));
            i++;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            workbook.write(outputStream);
            String fileName = UUID.randomUUID().toString().replaceAll("-", "").trim() + ".xlsx";
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            String result = FileUtils.uploadFile(inputStream, fileName);
            JSONObject jsonObject = JSONObject.parseObject(result);
            String mediaId = jsonObject.getString("media_id");
            String path = jsonObject.getString("path");
            domain.setIccId(iccId).setType(type).setMediaId(mediaId)
                    .setUpdateTime(new Date()).setFileName(fileName)
                    .setErrorCount(rows.keySet().size()).setCreateTime(new Date())
                    .setPath(path).setSpId(spId);
            errorFileMapper.save(domain);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCellValue(Cell cell) {
        if (cell == null) return null;
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
            return String.valueOf(cell.getNumericCellValue()).trim();
        if (cell.getCellType() == Cell.CELL_TYPE_STRING)
            return cell.getStringCellValue().trim();
        return null;
    }

    @Async
    public void importCard(InputStream inputStream, String spId) {
        Map<Row, String> errorMap = new LinkedHashMap<>();
        Workbook workbook;
        try {
            workbook = new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return;
        }
        Sheet sheet = workbook.getSheetAt(0);
        int firstRowNum = sheet.getFirstRowNum();
        int lastRowNum = sheet.getLastRowNum();
        for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
            Row row = sheet.getRow(i);
            String iccId = getCellValue(row.getCell(0));
            String phone = getCellValue(row.getCell(4));
            String username = getCellValue(row.getCell(5));
            if (StringUtils.isBlank(iccId)) {
                errorMap.put(row, "iccId不可为空");
                continue;
            }
            if (StringUtils.isBlank(phone) || phone.length() != 11) {
                errorMap.put(row, "请填入正确的手机号信息(11位)");
                continue;
            }
            if (StringUtils.isBlank(username)) {
                errorMap.put(row, "姓名必填");
                continue;
            }
            CardDomain cardDomain = new CardDomain();
            cardDomain.setSpId(spId).setIccId(iccId)
                    .setRemark(getCellValue(row.getCell(1)))
                    .setKeyWord(getCellValue(row.getCell(2)));
            CardDomain domain = cardMapper.findByIccId(iccId);
            if (domain == null) {
                errorMap.put(row, "物联网卡不存在,或者为与jasper平台进行数据同步,请点击同步按钮进行数据同步后在试");
                continue;
            }
            domain = cardMapper.findByKeyWord(cardDomain);
            if (domain != null && !iccId.equals(domain.getIccId())) {
                errorMap.put(row, "该关键子已经被其他物联网卡使用");
                continue;
            }
            if (phoneMapper.findByPhone(iccId, phone) != null) {
                errorMap.put(row, "该手机已经被本物联网卡绑定过,请勿重新绑定");
                continue;
            }

            String groupName = getCellValue(row.getCell(3));
            Map<String, Object> map = cardGroupMapper.findByName(groupName, spId);
            int groupId;
            if (map == null || map.isEmpty()) {
                groupId = cardGroupMapper.save(new CardGroupDomain().setName(groupName)
                        .setCreateTime(new Date()).setSpId(spId));
            } else {
                groupId = Integer.parseInt(String.valueOf(map.get("id")));
            }
            cardDomain.setGroupId(groupId);
            PhoneDomain phoneDomain = new PhoneDomain();
            phoneDomain.setIccId(iccId).setPhone(phone).setUsername(username);
            cardMapper.update(cardDomain);
            phoneMapper.save(phoneDomain);

        }
        saveErrorFile(errorMap, cardHeaders, spId,null, 0);
    }

    @Async
    public void importMdn(InputStream inputStream,String spId, String iccId) {
        Map<Row, String> errorMap = new LinkedHashMap<>();
        Workbook workbook;
        try {
            workbook = new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return;
        }
        Sheet sheet = workbook.getSheetAt(0);
        int firstRowNum = sheet.getFirstRowNum();
        int lastRowNum = sheet.getLastRowNum();
        System.out.println("11111111");
        System.out.println("firstRow" + firstRowNum);
        System.out.println("lastRowNum" + lastRowNum);
        for (int i = firstRowNum + 1; i <= lastRowNum; i++) {
            Row row = sheet.getRow(i);
            if (row.getCell(0) == null) {
                errorMap.put(row, "手机号必填(11位)");
                continue;
            }
            if (row.getCell(1) == null) {
                errorMap.put(row, "姓名不可为空");
                continue;
            }
            String phone = row.getCell(0).getStringCellValue();
            String username = row.getCell(1).getStringCellValue();
            if (StringUtils.isBlank(phone) || phone.length() != 11) {
                errorMap.put(row, "手机号必填(11位)");
                continue;
            }
            if (StringUtils.isBlank(username)) {
                errorMap.put(row, "姓名不可为空");
                continue;
            }
            CardDomain domain = cardMapper.findByIccId(iccId);
            if (domain == null) {
                errorMap.put(row, "物联网卡不存在,或者未与jasper平台进行数据同步,请点击同步按钮进行数据同步后重试");
                continue;
            }
            if (phoneMapper.findByPhone(iccId, phone) != null) {
                errorMap.put(row, "该手机已经被本物联网卡绑定过,请勿重新绑定");
                continue;
            }
            PhoneDomain phoneDomain = new PhoneDomain();
            phoneDomain.setIccId(iccId).setPhone(phone).setUsername(username);
            phoneMapper.save(phoneDomain);
        }
        saveErrorFile(errorMap, phoneHeaders,spId, iccId, 0);
    }

}
