package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardUpdateInputDTO {

    @NotNull(message = "iccId不可为空")
    private String iccId;

    private String spId;

    private String remark;

    private String keyWord;

    private int groupId;

    private String address;

    private int addressCode;

    private String longitude;

}
