package cn.com.flaginfo.config;

import cn.com.flaginfo.common.util.SystemMessage;
import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@Configuration
@SuppressWarnings("unchecked")
public class DruidDBConfig {

    @Bean
    public DataSource dataSource(){
        DruidDataSource dataSource=new DruidDataSource();
        dataSource.setUrl(SystemMessage.getString("spring.datasource.url"));
        dataSource.setUsername(SystemMessage.getString("spring.datasource.username"));
        dataSource.setPassword(SystemMessage.getString("spring.datasource.password"));
        dataSource.setDriverClassName(SystemMessage.getString("spring.datasource.driverClassName"));
        dataSource.setInitialSize(SystemMessage.getInteger("spring.datasource.initialSize"));
        dataSource.setMinIdle(SystemMessage.getInteger("spring.datasource.minIdle"));
        dataSource.setMaxActive(SystemMessage.getInteger("spring.datasource.maxActive"));
        dataSource.setMaxWait(SystemMessage.getInteger("spring.datasource.maxWait"));
        dataSource.setTimeBetweenEvictionRunsMillis(SystemMessage.getInteger("spring.datasource.timeBetweenEvictionRunsMillis"));
        dataSource.setMinEvictableIdleTimeMillis(SystemMessage.getInteger("spring.datasource.minEvictableIdleTimeMillis"));
        dataSource.setValidationQuery(SystemMessage.getString("spring.datasource.validationQuery"));
        dataSource.setTestWhileIdle(SystemMessage.getBoolean("spring.datasource.testWhileIdle"));
        dataSource.setTestOnBorrow(SystemMessage.getBoolean("spring.datasource.testOnBorrow"));
        dataSource.setTestOnReturn(SystemMessage.getBoolean("spring.datasource.testOnReturn"));
        dataSource.setPoolPreparedStatements(SystemMessage.getBoolean("spring.datasource.poolPreparedStatements"));
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(SystemMessage.getInteger("spring.datasource.maxPoolPreparedStatementPerConnectionSize"));
        return dataSource;
    }
}
