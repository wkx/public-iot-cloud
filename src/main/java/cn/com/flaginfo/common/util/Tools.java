package cn.com.flaginfo.common.util;

import cn.com.flaginfo.ApiClient;
import cn.com.flaginfo.common.support.IotException;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by wkx on 2017/3/8.
 *
 * @author wkx
 */
public class Tools {

    private static final Logger LOGGER = LoggerFactory.getLogger(Tools.class);

    public static boolean isParamEmpty(Map<String, Object> params, String... keys) {
        if (params == null) return false;
        if (keys == null) return true;
        for (String key : keys) {
            if (params.get(key) == null) return true;
        }
        return false;
    }

    public static JSONObject postForJson(String urlKey, Map<String, Object> params) {
        ApiClient client=new ApiClient(urlKey,SystemMessage.getString("apiKey"),"1.0");
        String jsonBody=JSONObject.toJSONString(params);
        client.setJsonBody(JSONObject.toJSONString(params));
        String result = client.postAsJson();
        LOGGER.info("post:"+urlKey+";params:"+jsonBody+";response:"+result);
        return JSONObject.parseObject(result);
    }

    public static void ExportExcel(JSONArray array, String headers[], String columns[], HttpServletResponse response) {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row row = sheet.createRow(0);
        int length = headers.length;
        for (int i = 0; i < length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(headers[i]);
        }
        length = array.size();
        int columnsLength = columns.length;
        for (int i = 0; i < length; i++) {
            JSONObject json = array.getJSONObject(i);
            row = sheet.createRow(i + 1);
            for (int j = 0; j < columnsLength; j++) {
                Cell cell = row.createCell(j);
                cell.setCellValue(json.getString(columns[j]));
            }
        }
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            String filename= UUID.randomUUID().toString().replace("-","").trim()+".xlsx";
            response.setHeader("content-disposition", "attachment;filename=" +filename+ ".xlsx");
            workbook.write(response.getOutputStream());

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IotException("499","文件导出失败");
        }
    }

    public static long timeStamp(){
        return System.currentTimeMillis()/1000;
    }

}
