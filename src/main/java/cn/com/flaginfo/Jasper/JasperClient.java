package cn.com.flaginfo.Jasper;

import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.common.util.Constant;
import cn.com.flaginfo.common.util.HttpClientPool;
import cn.com.flaginfo.common.util.Tools;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/6.
 *
 * @author wkx
 */
public class JasperClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(JasperClient.class);

    private static String httpGetMethod(HttpClient client, String url) {
        try {
            HttpGet httpGet = new HttpGet(url);
            RequestConfig config = RequestConfig.custom().setSocketTimeout(10000).
                    setConnectTimeout(10000).build();
            httpGet.setConfig(config);
            HttpResponse DataResponse = client.execute(httpGet);
            String result = EntityUtils.toString(DataResponse.getEntity(), "UTF-8");
            LOGGER.info("jasper平台同步获取的信息：{}", result);
            if (DataResponse.getStatusLine().getStatusCode() == org.apache.http.HttpStatus.SC_OK) {
                return result;
            }
            throw new IotException("499", "jasper平台同步获取的信息失败:" + result);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IotException("499", "jasper平台同步获取的信息失败");
        }
    }

    private static String httpPostMethod(HttpClient client, String url, List<NameValuePair> params) {
        HttpPost post = new HttpPost(url);
        RequestConfig config = RequestConfig.custom().setSocketTimeout(10000).
                setConnectTimeout(10000).build();
        post.setConfig(config);
        try {
            post.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = client.execute(post);
            String resultStr = EntityUtils.toString(response.getEntity());
            LOGGER.info("jasper平台同步获取的信息：{}", resultStr);
            if (response.getStatusLine().getStatusCode() != org.apache.http.HttpStatus.SC_OK)
                throw new IotException("499", "jasper平台同步获取的信息失败:" + resultStr);
            return resultStr;
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IotException("499", "jasper平台同步获取的信息失败");
        }
    }

    public static CookieStore getClient(String username, String password) {
        LOGGER.info("账号信息：{},{}", username, password);
        CookieStore cookieStore = new BasicCookieStore();
        HttpClient client = HttpClientPool.getHttpClient(cookieStore);
        try {
            LOGGER.info("进入登陆页面,获取模拟登陆cookie信息");
            httpGetMethod(client, Constant.JASPER_LOGIN_URL);
            LOGGER.info("进行安全信息认证,获得模拟登陆授权");
            HttpPost securityPost = new HttpPost(Constant.JASPER_SECURITY_URL);
            RequestConfig config = RequestConfig.custom().setSocketTimeout(10000).
                    setConnectTimeout(10000).build();
            securityPost.setConfig(config);
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("j_username", username));
            params.add(new BasicNameValuePair("j_password", password));
            securityPost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse securityResponse = client.execute(securityPost);
            if (securityResponse.getStatusLine().getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
                Header headers[] = securityResponse.getHeaders("Location");
                if (headers.length != 1) throw new IotException("499", "账号模拟登陆失败");
                if (headers[0].getValue().equals(Constant.JASPER_LOGIN_SUCCESS))
                    return cookieStore;
            }
            throw new IotException("499", "账号模拟登陆失败");
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IotException("499", "账号模拟登陆失败");
        }
    }

    public static String getJasperUserInfo(HttpClient client){
        String url=String.format(Constant.JASPER_USER_INFO,Tools.timeStamp());
        return httpGetMethod(client,url);
    }

    public static Map<String, Object> getLicense(HttpClient client) {
        String resultStr = httpGetMethod(client, Constant.JASPER_License_URL);
        int apiBegin = resultStr.indexOf("window.apiHost") + 18;
        int apiEnd = resultStr.indexOf(";", apiBegin) - 1;
        String apiHost = resultStr.substring(apiBegin, apiEnd);
        int licenseKeyBegin = resultStr.indexOf("window.apiLicenseKey") + 24;
        int licenseKeyEnd = resultStr.indexOf(";", licenseKeyBegin) - 1;
        String apiLicenseKey = resultStr.substring(licenseKeyBegin, licenseKeyEnd);
        Map<String, Object> result = new HashMap<>();
        result.put("apiHost", apiHost);
        result.put("apiLicenseKey", apiLicenseKey);
        return result;
    }

    public static String simList(HttpClient client, Integer totalCount) {
        int pageLimit = totalCount != null ? totalCount : 2;
        String url = String.format(Constant.JASPER_CARD_LIST, Tools.timeStamp(), 1, pageLimit);
        LOGGER.info("进行物联网卡信息数据同步");
        return httpGetMethod(client, url);
    }

    private static String getFbRatePlanIdFromRatePlanId(HttpClient client,String ratePlanId){
        String url = String.format(Constant.JASPER_GET_RATE_PLANID, Tools.timeStamp(),ratePlanId);
        return httpGetMethod(client,url);
    }

    public static String ratePlanDetail(HttpClient client, String ratePlanId) {
        String result=getFbRatePlanIdFromRatePlanId(client,ratePlanId);
        String url = String.format(Constant.JASPER_RATE_PLAN_DETAIL, result, Tools.timeStamp());
        LOGGER.info("进行资费信息数据同步");
        return httpGetMethod(client, url);
    }

    public static String cardDetail(HttpClient client, String simId) {
        JSONArray searchJson = new JSONArray();
        JSONObject param = new JSONObject();
        param.put("property", "simId");
        param.put("type", "LONG_EQUALS");
        param.put("value", simId);
        param.put("id", "simId");
        searchJson.add(param);
        String url;
        try {
            url = String.format(Constant.JASPER_CARD_DETAIL,
                    URLEncoder.encode(searchJson.toJSONString(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new IotException("编码格式错误", e);
        }
        LOGGER.info("获取物联网卡数据详情信息");
        return httpGetMethod(client, url);
    }

    public static String updateSim(HttpClient client, JSONArray paramArray) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("simChangesList", paramArray.toJSONString()));
        return httpPostMethod(client, Constant.JASPER_CARD_UPDATE, params);
    }

    public static String roleList(HttpClient client, int page, int limit, String search) {
        String url = String.format(Constant.JASPER_RULE_LIST, Tools.timeStamp(), page, limit);
        if (!StringUtils.isBlank(search)) {
            try {
                url = url + "&search=" + URLEncoder.encode(search, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                LOGGER.error(e.getMessage(), e);
                throw new IotException("499", "url编码错误");
            }
        }
        return httpGetMethod(client, url);
    }

    public static String enableRule(HttpClient client, String id) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", String.valueOf(id)));
        return httpPostMethod(client,Constant.JASPER_RULE_ENABLE,params);
    }

    public static String disableRule(HttpClient client,String id){
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", String.valueOf(id)));
        return httpPostMethod(client,Constant.JASPER_RULE_DISABLE,params);
    }

    public static String deleteRule(HttpClient client,String ids){
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("ids", String.valueOf(ids)));
        return httpPostMethod(client,Constant.JASPER_RULE_DELETE,params);
    }

    public static String createRule(HttpClient client, String code, String callbackUrl, String description, Integer filterAcctId) {
        String url =String.format(Constant.JASPER_RULE_CREATE,Tools.timeStamp());
        HttpPost post = new HttpPost(url);
        JSONObject requestJson = new JSONObject();
        requestJson.put("param0", code);//规则名称
        requestJson.put("callbackUrl", callbackUrl);
        requestJson.put("description", description);
        requestJson.put("filterAcctId", filterAcctId);//用户的acctId
        requestJson.put("filterOperatorId",9417);
        //默认配置
        JSONObject filtersDtoJson = new JSONObject();
        filtersDtoJson.put("customerId", null);//资费ID
        filtersDtoJson.put("statusId", null);//状态ID 11:可激活  6：已激活 7：已停用 8：已失效 16：全球转移中
        requestJson.put("filtersDto", filtersDtoJson);
        requestJson.put("actionTypeName", "PUSH_API");
        requestJson.put("content", "");
        requestJson.put("followUpActionEnabled", false);//跟进操作,意义不明，对本系统没有什么意义
        requestJson.put("deferActionScheduleParam0", "1");//延迟操作，意义不明，没有意义
        requestJson.put("eventTypeName", "SMS_MO_RECEIVED");
        requestJson.put("triggerCategoryName", "USAGE");//触发类型的名字，由eventTypeName决定。
        requestJson.put("deferAction", false);//延迟操作，意义不明
        requestJson.put("enabled", true);//激活
        try {
            post.setEntity(new StringEntity(requestJson.toJSONString(), "UTF-8"));
            post.setHeader(new BasicHeader("Content-Type", "application/json"));
            HttpResponse response = client.execute(post);
            int httpCode = response.getStatusLine().getStatusCode();
            System.out.println("获取的httpCode为：" + httpCode);
            if(response.getStatusLine().getStatusCode()== org.apache.http.HttpStatus.SC_OK)
                return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void updateRule(HttpClient client, int ruleId, String code,String callbackUrl, String description,String preJson) {
        String url = "https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m/" + ruleId + "?_dc=1483516532729";
        JSONObject requestJson = JSONObject.parseObject(preJson);
        requestJson.remove("callbackUrl");
        requestJson.remove("actionDesc");
        requestJson.remove("description");
        requestJson.remove("param0");
        requestJson.remove("paramDesc");
        requestJson.put("callbackUrl", callbackUrl);
        requestJson.put("actionDesc", "发出 API 消息到 " + callbackUrl);
        requestJson.put("description", description);
        requestJson.put("param0", code);
        requestJson.put("paramDesc", "简短代码 " + code);
        JSONObject filtersDtoJson = new JSONObject();
        filtersDtoJson.put("customerId", null);//资费ID
        filtersDtoJson.put("statusId", null);//状态ID 11:可激活  6：已激活 7：已停用 8：已失效 16：全球转移中
        requestJson.put("filtersDto", filtersDtoJson);
        try {
            HttpPost post = new HttpPost(url);
            post.setHeader(new BasicHeader("Content-Type", "application/json"));
            HttpResponse response = client.execute(post);
            int httpCode = response.getStatusLine().getStatusCode();
            System.out.println("获取的httpCode为：" + httpCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String ratePlaneList(HttpClient client, int page, int limit, String search){
        String url= String.format(Constant.JASPER_RATEPLAN_LIST,Tools.timeStamp(),page,limit);
        if (!StringUtils.isBlank(search)) {
            try {
                url = url + "&search=" + URLEncoder.encode(search, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new IotException("499", "url编码错误");
            }
        }
        return httpGetMethod(client, url);
    }

    public static String getPackageName(HttpClient client, String ratePlanId) {
        try {
            LOGGER.info("获取的ratePlanId为：{}",ratePlanId);
            String result = JasperClient.ratePlanDetail(client, ratePlanId);
            JSONObject resultJson = JSONObject.parseObject(result);
            StringBuilder sb=new StringBuilder("");
            JSONObject data=resultJson.getJSONArray("data").getJSONObject(0);
            String tier=data.getString("ratingTiers");
            sb.append(tier).append("档");
            JSONObject dataJson=data.getJSONObject("fbPricePlanDTOs").getJSONArray("D").
                    getJSONObject(0).getJSONArray("fbPlanThresholds").getJSONObject(0);
            sb.append(dataJson.getString("includedUsage")).append("MB");
            JSONArray smsJsonArry=data.getJSONObject("fbPricePlanDTOs").getJSONArray("SMO").
                    getJSONObject(0).getJSONArray("fbPlanThresholds");
            int sms=0;
            for(Object obj:smsJsonArry){
                JSONObject json= (JSONObject) obj;
                sms=sms+json.getIntValue("includedUsage");
            }
            sb.append(sms).append("条MO短信");
            return sb.toString();
        }catch (Exception e){
            LOGGER.info("资费计划名称解析错误");
            LOGGER.error(e.getMessage(),e);
        }
        return null;
    }
}
