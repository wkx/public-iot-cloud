package cn.com.flaginfo.common.util;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
public class Constant {

    public static final String JASPER_LOGIN_URL="https://chinaunicom.jasperwireless.com/provision/jsp/login.jsp";

    public static final String JASPER_LOGIN_SUCCESS="https://chinaunicom.jasperwireless.com/provision/action/launch";

    public static final String JASPER_SECURITY_URL="https://chinaunicom.jasperwireless.com/provision/j_acegi_security_check";

    public static final String JASPER_USER_INFO="https://chinaunicom.jasperwireless.com/provision/api/v1/users/searchUserProfile?_dc=%s&page=1&limit=1&sort=login&dir=ASC";

    public static final String JASPER_License_URL="https://chinaunicom.jasperwireless.com/provision/secure/apidoc/";

    public static final String JASPER_CARD_LIST="https://chinaunicom.jasperwireless.com/provision/api/v1/sims?_dc=%s&page=%s&limit=%s&sort=dateAdded&dir=DESC";

    public static final String JASPER_GET_RATE_PLANID="https://chinaunicom.jasperwireless.com/provision/api/v1/sims/getFbRatePlanIdFromRatePlanId?_dc=%s&ratePlanId=%s";

    public static final String JASPER_RATE_PLAN_DETAIL="https://chinaunicom.jasperwireless.com/provision/api/v1/ratePlans/ratePlanDetails/%s?_dc=%s";

    public static final String JASPER_CARD_DETAIL="https://chinaunicom.jasperwireless.com/provision/api/v1/sims/searchDetails?_dc=1488938482587&page=1&limit=50&search=%s";

    public static final String JASPER_CARD_UPDATE="https://chinaunicom.jasperwireless.com/provision/api/v1/sims/update/";

    public static final String JASPER_RULE_LIST="https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m?" + "_dc=%s&page=%s&limit=%s&sort=acctName&dir=ASC";

    public static final String JASPER_RULE_ENABLE="https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m/enableRule";

    public static final String JASPER_RULE_DISABLE="https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m/disableRule";

    public static final String JASPER_RULE_DELETE="https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m/deleteRule";

    public static final String JASPER_RULE_CREATE="https://chinaunicom.jasperwireless.com/provision/api/v1/pushapi/m2m?_dc=%s";

    public static final String JASPER_RATEPLAN_LIST="https://chinaunicom.jasperwireless.com/provision/api/v1/ratePlans?_dc=%s&page=%s&limit=%s&sort=name&dir=ASC";

    public static final String SUCCESS_CODE="200";

}
