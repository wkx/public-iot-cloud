package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class SmsTemplateListDTO {

    private String content;

    private String instruct;

    private String status;

    private int pageNo;

    private int pageLimit;

}
