package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors
public class AccountInputDTO {

    @NotNull(message = "用户名不可为空")
    private String username;

    @NotNull(message = "密码不可为空")
    private String password;

    @NotNull(message = "密钥不可为空")
    private String licenseKey;

    @NotNull(message = "apiUrl不可为空")
    private String apiUrl;

    private String remark;
}
