package cn.com.flaginfo.model.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;


/**
 * Created by wkx on 2017/3/13.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class AccountDomain {

    private int id;

    private String spId;

    private String username;

    private String password;

    private String licenseKey;

    private String apiUrl;

    private String remark;

    private Date createTime;

}
