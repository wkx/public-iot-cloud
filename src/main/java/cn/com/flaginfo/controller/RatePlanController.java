package cn.com.flaginfo.controller;

import cn.com.flaginfo.Jasper.JasperClient;
import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.common.util.Tools;
import cn.com.flaginfo.model.dto.RatePlanListDTO;
import cn.com.flaginfo.service.RatePlanService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@RestController
@RequestMapping("/ratePlan")
public class RatePlanController {

    @Autowired
    private RatePlanService ratePlanService;

    @RequestMapping("/synchronize")
    public JsonView synchronize(HttpSession session){
        String spId= SessionHolder.getSpId(session);
        HttpClient client=SessionHolder.getJasperClient(session);
        ratePlanService.synchronize(spId,client);
        return JsonView.getSuccView();
    }

    @RequestMapping("/list")
    public JsonView list(RatePlanListDTO dto,HttpSession session){
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        Map<String,Object> map=ratePlanService.findPage(dto);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.addAttribute("data",map);
        return jsonView;
    }

    @RequestMapping("/exportExcel")
    public void exportExcel(RatePlanListDTO dto, HttpSession session, HttpServletResponse response){
        String spId=SessionHolder.getSpId(session);
        dto.setSpId(spId);
        List<Map<String,Object>> list=ratePlanService.findAll(dto);
        String listStr= JSONObject.toJSONString(list);
        String []headers=new String[8];
        String []columns=new String[8];
        headers[0]="套餐名";headers[1]="计划名称";
        headers[2]="计划ID";headers[3]="版本ID";
        headers[4]="账户名";headers[5]="每个订户收费";
        headers[6]="计划内流量（MB）";headers[7]="付款方式";
        columns[0]="packageName";columns[0]="ratePlanName";
        columns[0]="ratePlanId";columns[0]="versionId";
        columns[0]="acctName";columns[0]="charge";
        columns[0]="dataUsage";columns[0]="payMethod";
        Tools.ExportExcel(JSONArray.parseArray(listStr),headers,columns,response);
    }

    @RequestMapping("/detail")
    public JsonView detail(@RequestParam("ratePlanId")String ratePlanId,HttpSession session){
        HttpClient client=SessionHolder.getJasperClient(session);
        String detail=JasperClient.ratePlanDetail(client,ratePlanId);
        JsonView jsonView=JsonView.getSuccView();
        jsonView.put("data",JSONObject.parseObject(detail).getJSONArray("data"));
        return jsonView;
    }
}
