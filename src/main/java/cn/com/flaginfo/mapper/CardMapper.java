package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.CardDomain;
import cn.com.flaginfo.model.dto.CardListInputDTO;
import cn.com.flaginfo.model.dto.MapListDTO;
import cn.com.flaginfo.model.pojo.CmcSmsReply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/8.
 * @author wkx
 */
@Mapper
public interface CardMapper {

    void save(CardDomain domain);

    List<Map<String,Object>> findPage(CardListInputDTO dto);

    int finCount(CardListInputDTO dto);

    void update(CardDomain dto);

    CardDomain findByKeyWord(CardDomain dto);

    CardDomain findByIccId(String iccId);

    Map<String,Object> findByKeyWordAndPhone(CmcSmsReply reply);

    List<Map<String,Object>> findByGroupNameAndPhone(CmcSmsReply reply);

    void delete(String iccId);

    void updateAllSynchronize(String spId);

    void deleteAllSynchronize(String spId);

    List<Map<String,Object>> findPageByMap(MapListDTO dto);

    List<Map<String,Object>> findAll(CardDomain domain);

}
