package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardExportDTO {

    private String spId;

    private String iccId;

    private String msiSdn;

    private String remark;

    private String keyWord;

    private Integer groupId;

}
