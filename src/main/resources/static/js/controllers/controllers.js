/**
 * Created by Chuck on 17/3/14.
 */
// var rootHttp = 'http://'+window.location.host;
var rootHttp = 'http://10.0.0.139:8091';
var httpGet=function (url,successFunc) {
    $.ajax({
        method:'GET',
        url:rootHttp + url,
        type:'json',
        cache: false,
        success: function (res) {
            successFunc(res);
        }
    });
};

var httPost=function (url,data,successFunc) {
    $.ajax({
        method:"POST",
        url:rootHttp+url,
        cache:false,
        data:data,
        success:function(res){
            successFunc(res);
        }
    })
};
angular.module('mainController',[])
    .controller("accountController",['$scope','$rootScope','$uibModal','$state',function($scope,$rootScope,$uibModal,$state){
        //获取用户是否登录

        $scope.bindAccount=false;
        $scope.accountInfo=false;

        angular.element('#loading').show();

        httpGet("/account/authorize",function (res) {
            angular.element('#loading').hide();
            var data = res.data;
            if (data && data.bind) {
                $scope.accountInfo = true;
                $scope.account = res.data.account;
                $scope.$apply();
            } else {
                $scope.bindAccount = true;
                $rootScope.$on('$stateChangeStart',function(event){
                    // 如果用户不存在
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '请先绑定Jasper平台企业账号';
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    event.preventDefault();// 取消默认跳转行为
                    $state.go("account");//跳转到登录界面
                });
                $scope.$apply();
            }
        });

        //未成功跳转页面
        $scope.submitAccount = function(){
            if($scope.account==null||$scope.account==undefined){
                alert("请输入内容");
                return;
            }
            if($scope.account.username==null||$scope.account.username==undefined){
                alert("请输入企业用户名");
                return;
            }
            if($scope.account.password==null||$scope.account.password==undefined){
                alert("请输入账户密码");
                return;
            }
            if($scope.account.licenseKey==null||$scope.account.licenseKey==undefined){
                alert("请输入企业licenseKey");
                return;
            }
            if($scope.account.apiUrl==null||$scope.account.apiUrl==undefined){
                alert("请输入API server");
                return;
            }
            var data = {
                "username": $scope.account.username,
                "password": $scope.account.password,
                "licenseKey": $scope.account.licenseKey,
                "apiUrl": $scope.account.apiUrl,
                "remark": $scope.account.remark
            };
            $rootScope.hintmodal = $uibModal.open({
                templateUrl:'html/hintTemplate.html',
                controller:function($scope,$rootScope){
                    $scope.confirm = '绑定成功后，将无法更换其他企业账号。';
                    $scope.ok = function(){
                        angular.element('#loading').show();
                        $rootScope.hintmodal.close();
                        //发送绑定请求
                        httPost("/account/bind",data, function(res){
                            angular.element('#loading').hide();
                            var data = res.data;
                            if(res.returnCode == "200"){ //确认绑定后跳转
                                window.location.reload();
                            }else{
                                $rootScope.hintmodal = $uibModal.open({
                                    templateUrl: 'html/hintTemplate.html',
                                    controller: function($scope,$rootScope){
                                        $scope.confirm = res.returnMsg+',请重新输入账号信息';
                                        $scope.ok = function () {
                                            $rootScope.hintmodal.close();
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }) ;
        };
    }])
    .controller('cardController',['$scope','$rootScope','$http','$location','$uibModal',function($scope,$rootScope,$http,$location,$uibModal){
        var data = {
            pageNo: 1,
            pageLimit: 10
        };
        $.ajax({
            method:'POST',
            url:rootHttp + '/card/list',
            type:'json',
            data: data,
            cache: false,
            success: function(data){
                $rootScope.reportData = data.data.list;
                $scope.totalItems = data.data.dataCount;
                $scope.$apply();
            },
            error: function (data) {
                // error
            }
        });
        $scope.maxSize = 3;       //可选页数范围
        $scope.currentPage = 1;   //当前页数
        $scope.totalItems = 0;    //总页数
        $scope.perPage = 10;      //每页显示多少条

        $.ajax({
            method:'GET',
            url:rootHttp + '/card/group/list',
            type:'json',
            cache: false,
            success: function(data){
                $rootScope.groupInfo = data.data;
                $scope.$apply();
            },
            error: function (data) {
                // error
            }
        });

        $scope.selectedId = 'ICCID';

        //全选事件

        $scope.count = 0;//已选择数量
        $scope.selectData = [];//已选对象


        //选择单个（取消选择单个
        $scope.changeCurrent = function(current, $event) {
            //计算已选数量 true加， false减
            $scope.count += current.checked ? 1 : -1;
            //判断是否全选，选数量等于数据长度为true
            $scope.selectAll = $scope.count === $rootScope.reportData.length;
            //统计已选对象
            $scope.selectData = [];
            angular.forEach($rootScope.reportData, function(item) {
                if(item.checked){
                    $scope.selectData[$scope.selectData.length] = item;
                }
            });
            console.log($scope.selectData)
            $event.stopPropagation();//阻止冒泡

        };
        //单击行选中
        // $scope.changeCurrents = function(current, $event) {
        //     if(current.checked == undefined){
        //         current.checked = true;
        //     }else{
        //         current.checked = !current.checked;
        //     }
        //     $scope.changeCurrent(current, $event);
        // };
        //全选（取消全选
        $scope.changeAll = function() {

            $rootScope.selectAll = $scope.selectAll;

            angular.forEach($rootScope.reportData, function(item) {
                item.checked = $scope.selectAll;
            });

            $scope.count = $scope.selectAll ? $rootScope.reportData.length : 0;
            if ($scope.selectAll) {
                $scope.selectData = $rootScope.reportData;
            } else {
                $scope.selectData = [];
            }
        };

        //分页事件
        $scope.pageChanged = function () {
            var data = {
                pageNo: $scope.currentPage,
                pageLimit: $scope.perPage,
                remark: $scope.searchRemark,
                KeyWrd: $scope.searchKey,
                groupId: $scope.selectedGroup
            };
            if ($scope.selectedId == 'ICCID') {
                $scope.searchId ? data.iccId = $scope.searchId : null;
            }else if ($scope.selectedId == 'MSISDN') {
                $scope.searchId ? data.msiSdn = $scope.searchId : null;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.reportData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };


        //   按钮点击事件

        // 导出按钮事件

        $scope.dataImport = function () {
            // var data = {
            //     pageNo: 1,
            //     pageLimit: 10,
            //     remark: $scope.searchRemark || '',
            //     KeyWrd: $scope.searchKey || '',
            //     groupId: $scope.selectedGroup? $scope.selectedGroup.id: ''
            //
            // };
            // if ($scope.selectedId == 'ICCID') {
            //     data.iccId = $scope.searchId || ''
            // }else if ($scope.selectedId == 'MSISDN') {
            //     data. msiSdn = $scope.searchId || ''
            // }
            // $.ajax({
            //     method:'POST',
            //     url:rootHttp + '/card/exportIccCard',
            //     type:'json',
            //     data:data,
            //     cache: false,
            //     success: function(data){
            //         console.log(data);
            //         window.location.href = 'http://10.0.0.139:8091/card/exportIccCard';
            //     },
            //     error: function (data) {
            //         // error
            //     }
            // });
            window.location.href = rootHttp + '/card/exportIccCard';
        };
        //  激活按钮事件
        $scope.active = function () {
            var simIds = [];
            for (var i = 0;i < $scope.selectData.length;i++) {
                var simIdItem = {};
                simIdItem.simId = $scope.selectData[i].simId;
                simIds.push(simIdItem);
            }
            var data = {
                simIdList: JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'DEACTIVATED_NAME'
            };
            if ($scope.selectData.length == 0) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择数据';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            for (var j = 0;j < $scope.selectData.length;j++) {
                if ($scope.selectData[j].status !== '已停用') {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '请选择状态为已停用的物联网卡';
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    return;
                }
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/active',
                data:data,
                type:'json',
                cache: false,
                success: function (res) {
                    if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = res.returnMsg;
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '激活成功';
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method:'POST',
                            url:rootHttp + '/card/list',
                            type:'json',
                            data:data,
                            cache: false,
                            success: function(data){
                                $rootScope.reportData = data.data.list;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function (data) {
                                // error
                            }
                        });
                        $scope.selectData = [];
                    }
                },
                error: function (res) {
                    //error
                }
            });
        };

        //停用按钮事件

        $scope.stop = function () {
            var simIds = [];
            for (var i = 0;i < $scope.selectData.length;i++) {
                var simIdItem = {};
                simIdItem.simId = $scope.selectData[i].simId;
                simIds.push(simIdItem);
            }
            var data = {
                simIdList: JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'ACTIVATED_NAME'
            };
            if ($scope.selectData.length == 0) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择数据';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            for (var j = 0;j < $scope.selectData.length;j++) {
                if ($scope.selectData[j].status !== '已激活') {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '请选择状态为已激活的物联网卡';
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    return;
                }
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/active',
                data:data,
                type:'json',
                cache: false,
                success: function (res) {
                    if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = res.returnMsg;
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '停用成功';
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method:'POST',
                            url:rootHttp + '/card/list',
                            type:'json',
                            data:data,
                            cache: false,
                            success: function(data){
                                $rootScope.reportData = data.data.list;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function (data) {
                                // error
                            }
                        });
                        $scope.selectData = [];
                    }
                },
                error: function (res) {
                    //error
                }
            });
        };
        // 搜索按钮事件
        $scope.search = function () {
            var data = {
                pageNo: 1,
                pageLimit: 10,
                remark: $scope.searchRemark,
                KeyWrd: $scope.searchKey,
                groupId: $scope.selectedGroup
            };
            if ($scope.selectedId == 'ICCID') {
                $scope.searchId ? data.iccId = $scope.searchId : null;
            }else if ($scope.selectedId == 'MSISDN') {
                $scope.searchId ? data.msiSdn = $scope.searchId : null;
            }
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/list',
                type:'json',
                data:data,
                cache: false,
                success: function(data){
                    $rootScope.reportData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        };
        $scope.detail = function (items) {
            $location.url('/cardDetail?id='+items.simId);
        };
        $scope.phone = function (items) {
            $location.url('/phoneManage?id='+items.iccId);
        };
        $scope.change = function (items) {
            //这里很关键,是打开模态框的过程
            $rootScope.modifyCard = items;
            $rootScope.cardChangemodal = $uibModal.open({
                templateUrl: 'html/editCard.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.message = function (items) {
            sessionStorage.setItem('cardMessage',JSON.stringify(items));
            $rootScope.cardMessagemodal = $uibModal.open({
                templateUrl: 'html/upMessage.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                controller: 'cardMessageController',
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.infoUpdate = function () {
            angular.element('#loading').show();
            var data = {
                pageNo: 1,
                pageLimit: 10
            };
            $.ajax({
                method:'GET',
                url:rootHttp + '/card/synchronize',
                type:'json',
                cache: false,
                success: function (res) {
                    angular.element('#loading').hide();
                    if (res.returnCode == '500') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            backdrop: 'static',
                            keyboard: false,
                            controller: function($scope,$rootScope){
                                $scope.confirm = res.returnMsg;
                                $scope.ok = function () {
                                    window.location.href = 'index.html#!/account';
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '200') {
                        $.ajax({
                            method:'POST',
                            url:rootHttp + '/card/list',
                            type:'json',
                            data:data,
                            cache: false,
                            success: function(data){
                                $rootScope.reportData = data.data.list;
                                $scope.totalItems = data.data.dataCount;
                                $rootScope.hintmodal = $uibModal.open({
                                    templateUrl: 'html/hintTemplate.html',
                                    controller: function($scope,$rootScope){
                                        $scope.confirm = '信息同步成功';
                                        $scope.ok = function () {
                                            $rootScope.hintmodal.close();
                                        }
                                    }
                                });
                                $scope.$apply();
                            },
                            error: function (data) {
                                // error
                            }
                        });
                    }
                }
            })
        };
        $scope.downloadTemp = function () {
            window.location.href = '/js/importTemplate/cardTemplate.xlsx'
        };
        $scope.tempBind = function () {
            $rootScope.tempBindmodal = $uibModal.open({
                templateUrl:'html/bulkImport.html',
                controller:'tempBindController',
                size:'middle',
                backdrop:'static',
                keyboard:false
            });
        }
    }])
    .controller('cardFindController',['$scope','$rootScope','$location','$http','$uibModal',function($scope,$rootScope,$location,$http,$uibModal){
        var data = {
            pageNo: 1,
            pageLimit: 5
        };
        $.ajax({
            method:'POST',
            url:rootHttp + '/card/map/list',
            type:'json',
            data: data,
            cache: false,
            success: function(data){
                $rootScope.cardFindData = data.data.list;
                $scope.totalItems = data.data.dataCount;
                if ($rootScope.cardFindData.length !== 0) {
                    var longitude = $rootScope.cardFindData[0].longitude.split(',')[0];
                    var latitude = $rootScope.cardFindData[0].longitude.split(',')[1];
                }
                init_map(longitude,latitude);
                // 向地图添加标注
                var length = $rootScope.cardFindData ? $rootScope.cardFindData.length : 0;
                for (var i = 0; i < length; i++) {
                    if ($rootScope.cardFindData) {
                        var lng = $rootScope.cardFindData[i].longitude.split(',')[0];
                        var lat = $rootScope.cardFindData[i].longitude.split(',')[1];
                        var point = new BMap.Point(lng,lat);
                        var id = $rootScope.cardFindData[i];
                        addMarker(point,id);
                    }
                }
                $scope.$apply();
            },
            error: function (data) {
                // error
            }
        });
        $scope.maxSize = 2;       //可选页数范围
        $scope.currentPage = 1;   //当前页数
        $scope.totalItems = 0;    //总页数
        $scope.perPage = 5;      //每页显示多少条

        //分页事件
        $scope.pageChanged = function () {
            var data = {
                pageNo: $scope.currentPage,
                pageLimit: $scope.perPage,
                addressCode: $scope.addressCode
            };
            if ($scope.selectedId == 'ICCID') {
                $scope.searchId ? data.iccId = $scope.searchId : null;
            }else if ($scope.selectedId == 'MSISDN') {
                $scope.searchId ? data.msiSdn = $scope.searchId : null;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/map/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.cardFindData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };
        $scope.selectedId = 'ICCID';

        // 三级联动设置
        $http.get('js/json/country.json').then(function (res) {
            $scope.regionData = res.data;
            $scope.citys = [];
            $scope.districts = [];
            $scope.regionText = {};
            $scope.$watch('selectedProvince',function(newValue,oldValue){
                if(newValue != oldValue){
                    var i = 0,len = $scope.regionData.length;
                    if(!newValue){ //判断选择的是否选择省份，如果没有则重置市区
                        $scope.citys = [];
                        $scope.districts = [];
                        return;
                    }
                    for(i;i < len;i ++){
                        if($scope.regionData[i].cityId == $scope.selectedProvince){
                            $scope.citys = $scope.regionData[i].children;
                            $scope.regionText.selectedProvinceText = $scope.regionData[i].name;
                        }
                    }
                    $scope.districts = [];
                }
            });

            $scope.$watch('selectedCity',function(newValue,oldValue){
                if(newValue != oldValue){
                    if(!newValue){ //作用同上
                        $scope.districts = [];
                        return;
                    }
                    var i = 0,len = $scope.citys.length;
                    for(i;i < len; i ++){
                        if($scope.citys[i].cityId == $scope.selectedCity){
                            $scope.districts = $scope.citys[i].children;
                            $scope.regionText.selectedCityText = $scope.citys[i].name;
                        }
                    }
                }
            });
            $scope.districtObj = {};
            $scope.$watch('selectedDstrict',function(newValue,oldValue){
                if(newValue != oldValue){
                    var i = 0,len = $scope.districts.length;
                    for(i;i < len ; i ++){
                        if($scope.districts[i].cityId == $scope.selectedDstrict){
                            $scope.districtObj = $scope.districts[i];
                            $scope.regionText.selectedDstrictText = $scope.districts[i].name;
                            $scope.addressCode = $scope.districts[i].cityId;
                            console.log($scope.addressCode)
                        }
                    }
                }
            });
        });

        // 列表下拉按钮操作
        $scope.listDetail = function (items) {
            $location.url('/cardDetail?id='+items.simId);
        };
        $scope.listManage = function (items) {
            $location.url('/phoneManage?id='+items.iccId);
        };
        $scope.listMessage = function (items) {
            sessionStorage.setItem('cardMessage',JSON.stringify(items));
            $rootScope.cardMessagemodal = $uibModal.open({
                templateUrl: 'html/upMessage.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                controller: 'cardMessageController',
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.listOperate = function (items) {
            var simIds = [{simId:items.simId}];
            var activeData = {
                simIdList:JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'ACTIVATED_NAME'
            };
            var stopData = {
                simIdList:JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'DEACTIVATED_NAME '
            };
            if (items.status == '已激活') {
                $.ajax({
                    method: 'POST',
                    url:rootHttp + '/card/active',
                    data:activeData,
                    type:'json',
                    cache: false,
                    success: function (res) {
                        if (res.returnCode == '499') {
                            $rootScope.hintmodal = $uibModal.open({
                                templateUrl: 'html/hintTemplate.html',
                                controller: function($scope,$rootScope){
                                    $scope.confirm = res.returnMsg;
                                    $scope.ok = function () {
                                        $rootScope.hintmodal.close();
                                    }
                                }
                            });
                        }else if (res.returnCode == '200') {
                            $rootScope.hintmodal = $uibModal.open({
                                templateUrl: 'html/hintTemplate.html',
                                controller: function($scope,$rootScope){
                                    $scope.confirm = '激活成功';
                                    $scope.ok = function () {
                                        $rootScope.hintmodal.close();
                                    }
                                }
                            });
                            $.ajax({
                                method:'POST',
                                url:rootHttp + '/card/map/list',
                                type:'json',
                                data:data,
                                cache: false,
                                success: function(data){
                                    $rootScope.cardFindData = data.data.list;
                                    $scope.totalItems = data.data.dataCount;
                                    $scope.$apply();
                                },
                                error: function (data) {
                                    // error
                                }
                            });
                        }
                    },
                    error: function (res) {
                        //error
                    }
                });
            } else {
                $.ajax({
                    method: 'POST',
                    url:rootHttp + '/card/active',
                    data:activeData,
                    type:'json',
                    cache: false,
                    success: function (res) {
                        if (res.returnCode == '499') {
                            $rootScope.hintmodal = $uibModal.open({
                                templateUrl: 'html/hintTemplate.html',
                                controller: function($scope,$rootScope){
                                    $scope.confirm = res.returnMsg;
                                    $scope.ok = function () {
                                        $rootScope.hintmodal.close();
                                    }
                                }
                            });
                        }else if (res.returnCode == '200') {
                            $rootScope.hintmodal = $uibModal.open({
                                templateUrl: 'html/hintTemplate.html',
                                controller: function($scope,$rootScope){
                                    $scope.confirm = '停用成功';
                                    $scope.ok = function () {
                                        $rootScope.hintmodal.close();
                                    }
                                }
                            });
                            $.ajax({
                                method:'POST',
                                url:rootHttp + '/card/map/list',
                                type:'json',
                                data:data,
                                cache: false,
                                success: function(data){
                                    $rootScope.cardFindData = data.data.list;
                                    $scope.totalItems = data.data.dataCount;
                                    $scope.$apply();
                                },
                                error: function (data) {
                                    // error
                                }
                            });
                        }
                    },
                    error: function (res) {
                        //error
                    }
                });
            }
        };
        $scope.listChange = function (items) {
            //这里很关键,是打开模态框的过程
            $rootScope.modifyCard = items;
            $rootScope.cardChangemodal = $uibModal.open({
                templateUrl: 'html/editCard.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        function init_map (lng,lat) {
            $scope.map = new BMap.Map("map",{enableMapClick:false});// 创建Map实例
            if (lng && lat) {
                $scope.map.centerAndZoom(new BMap.Point(lng,lat), 13);// 初始化地图,设置中心点坐标和地图级别
            }else {
                $scope.map.centerAndZoom(new BMap.Point(121.6031892716,31.2000579249), 13);// 初始化地图,设置中心点坐标和地图级别
            }

            var opts = {type: BMAP_NAVIGATION_CONTROL_SMALL,anchor: BMAP_ANCHOR_TOP_RIGHT};
            $scope.map.addControl(new BMap.NavigationControl(opts));
            $scope.map.setCurrentCity("上海");          // 设置地图显示的城市 此项是必须设置的
            $scope.map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        }

        // 编写自定义函数,创建标注
        function addMarker(point,id){
            var marker = new BMap.Marker(point);
            $scope.map.addOverlay(marker);
            var label;
            label = new BMap.Label(id, {offset: new BMap.Size(20, 0)}); //创建marker点的标记,这里注意下,因为百度地图可以对label样式做编辑,所以把重要的id放在了label(然后再隐藏)
            label.setStyle({display:"none"});//对label 样式隐藏
            marker.setLabel(label);
            marker.addEventListener("click", function (e) {
                event.stopPropagation();
                var data = e.target.getLabel().content;
                var ele = angular.element(document.getElementById('card-info-panel'));
                var offset = angular.element(".main-wrapper").offset();
                ele.css({"top":(e.clientY - offset.top)+ 5 +"px","left":(e.clientX - offset.left)+ 5 +"px"});
                ele.toggle();
                angular.element('body').click(function(){
                    ele.hide();
                });
                $scope.detail = function(){
                    $location.url('/cardDetail?id='+data.simId);
                };
                $scope.manage = function(){
                    $location.url('/phoneManage?id='+data.iccId);
                };
                $scope.message = function () {
                    sessionStorage.setItem('cardMessage',JSON.stringify(data));
                    $rootScope.cardMessagemodal = $uibModal.open({
                        templateUrl: 'html/upMessage.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                        controller: 'cardMessageController',
                        size: 'middle',//模态框的大小尺寸
                        backdrop: 'static',
                        keyboard: false
                    });
                };
                $scope.change = function () {
                    $rootScope.modifyCard = data;
                    $rootScope.cardChangemodal = $uibModal.open({
                        templateUrl: 'html/editCard.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                        size: 'middle',//模态框的大小尺寸
                        backdrop: 'static',
                        keyboard: false
                    });
                }

            });

        }

        // // 向地图添加标注
        // var length = $rootScope.cardFindData ? $rootScope.cardFindData.length : 0;
        // console.log($rootScope.cardFindData)
        // for (var i = 0; i < length; i++) {
        //     if ($rootScope.cardFindData) {
        //         var lng = $rootScope.cardFindData[i].longitude.split(',')[0];
        //         var lat = $rootScope.cardFindData[i].longitude.split(',')[1];
        //         var point = new BMap.Point(lng,lat);
        //         var id = $rootScope.cardFindData[i];
        //         addMarker(point,id);
        //     }
        // }

        //全选事件

        $scope.count = 0;//已选择数量
        $scope.selectData = [];//已选对象


        //选择单个（取消选择单个
        $scope.changeCurrent = function(current, $event) {
            //计算已选数量 true加， false减
            $scope.count += current.checked ? 1 : -1;
            //判断是否全选，选数量等于数据长度为true
            $scope.selectAll = $scope.count === $rootScope.cardFindData.length;
            //统计已选对象
            $scope.selectData = [];
            angular.forEach($rootScope.cardFindData, function(item) {
                if(item.checked){
                    $scope.selectData[$scope.selectData.length] = item;
                }
            });
            $event.stopPropagation();//阻止冒泡

        };

        //全选（取消全选
        $scope.changeAll = function() {

            $rootScope.selectAll = $scope.selectAll;

            angular.forEach($rootScope.cardFindData, function(item) {
                item.checked = $scope.selectAll;
            });

            $scope.count = $scope.selectAll ? $rootScope.cardFindData.length : 0;
            if ($scope.selectAll) {
                $scope.selectData = $rootScope.cardFindData;
            } else {
                $scope.selectData = [];
            }
        };

        //  激活按钮事件
        $scope.active = function () {
            var simIds = [];
            for (var i = 0;i < $scope.selectData.length;i++) {
                var simIdItem = {};
                simIdItem.simId = $scope.selectData[i].simId;
                simIds.push(simIdItem);
            }
            var data = {
                simIdList: JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'DEACTIVATED_NAME'
            };
            for (var j = 0;j < $scope.selectData.length;j++) {
                if ($scope.selectData[j].status !== '已激活') {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '请选择状态为已激活的物联网卡';
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    return;
                }
            }
            if ($scope.selectData.length == 0) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择数据';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/active',
                data:data,
                type:'json',
                cache: false,
                success: function (res) {
                    if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = res.returnMsg;
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '激活成功';
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method:'POST',
                            url:rootHttp + '/card/list',
                            type:'json',
                            data:data,
                            cache: false,
                            success: function(data){
                                $rootScope.cardFindData = data.data.list;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function (data) {
                                // error
                            }
                        });
                        $scope.selectData = [];
                    }
                },
                error: function (res) {
                    //error
                }
            });
        };

        //停用按钮事件

        $scope.stop = function () {
            var simIds = [];
            for (var i = 0;i < $scope.selectData.length;i++) {
                var simIdItem = {};
                simIdItem.simId = $scope.selectData[i].simId;
                simIds.push(simIdItem);
            }
            var data = {
                simIdList: JSON.stringify(simIds),
                changeType: 3,
                targetValue: 'ACTIVATED_NAME'
            };
            for (var j = 0;j < $scope.selectData.length;j++) {
                if ($scope.selectData[j].status !== '已停用') {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '请选择状态为已停用的物联网卡';
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    return;
                }
            }
            if ($scope.selectData.length == 0) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择数据';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/active',
                data:data,
                type:'json',
                cache: false,
                success: function (res) {
                    if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = res.returnMsg;
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '停用成功';
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method:'POST',
                            url:rootHttp + '/card/list',
                            type:'json',
                            data:data,
                            cache: false,
                            success: function(data){
                                $rootScope.cardFindData = data.data.list;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function (data) {
                                // error
                            }
                        });
                        $scope.selectData = [];
                    }
                },
                error: function (res) {
                    //error
                }
            });
        };

        //搜索按钮事件

        $scope.search = function () {
            var data = {
                pageNo:1,
                pageLimit:5,
                addressCode:$scope.addressCode
            };
            if ($scope.selectedId == 'ICCID') {
                $scope.searchId ? data.iccId = $scope.searchId : null;
            }else if ($scope.selectedId == 'MSISDN') {
                $scope.searchId ? data.msiSdn = $scope.searchId : null;
            }
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/map/list',
                type:'json',
                data: data,
                cache: false,
                success: function(data){
                    $rootScope.cardFindData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        }
    }])
    .controller('cardChangeController',['$scope','$rootScope','$http','$location','$uibModal',function($scope,$rootScope,$http,$location,$uibModal){
        // 初始化三级联动
        var longitude,
            latitude,
            items;
        items = $rootScope.modifyCard;
        if (items.longitude) {
            var log = items.longitude;
            longitude = log.split(',')[0];
            latitude = log.split(',')[1];
        }
        angular.element("#card-change-map").show();
        setTimeout(function(){init_map();},200);
        setTimeout(function(){
            angular.element('#address').val(items.address);
        },300);
        function init_map(){
            if (longitude && latitude) {
                public_map(longitude,latitude);
                // $scope.$apply();
            }else {
                public_map(121.6031892716,31.2000579249);
                // $scope.$apply();
            }
        }

        // 提取地图初始化公共方法，参数为经纬度
        function public_map(longitude,latitude){
            var point,
                marker;
            $scope.map = new BMap.Map("card-change-map",{enableMapClick:false});// 创建Map实例
            $scope.map.centerAndZoom(new BMap.Point(longitude, latitude), 17);// 初始化地图,设置中心点坐标和地图级别
            point = new BMap.Point(longitude,latitude);    //向地图添加标注
            marker = new BMap.Marker(point);
            $scope.map.addOverlay(marker);
            $scope.myGeo = new BMap.Geocoder();

            var opts = {type: BMAP_NAVIGATION_CONTROL_SMALL};
            $scope.map.addControl(new BMap.NavigationControl(opts));
            $scope.map.setCurrentCity("上海");          // 设置地图显示的城市 此项是必须设置的
            $scope.map.enableScrollWheelZoom(true);//开启鼠标滚轮缩放
            //下面为搜索地址自动填入功能
            function G(id) {
                return document.getElementById(id);
            }
            var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
                {"input" : "address",
                    "location" : $scope.map
                });
            ac.hide();
            ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
                var str = "";
                var _value = e.fromitem.value;
                var value = "";
                if (e.fromitem.index > -1) {
                    value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
                }
                str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

                value = "";
                if (e.toitem.index > -1) {
                    _value = e.toitem.value;
                    value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
                }
                str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
                G("searchResultPanel").innerHTML = str;
            });

            var myValue;
            ac.addEventListener("onconfirm", function(e) {//鼠标点击下拉列表后的事件
                // 地址解析初始化
                var myGeo = new BMap.Geocoder();
                var _value = e.item.value;
                myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
                //将地址解析为经纬度
                myGeo.getPoint(myValue, function(point){
                    if (point) {
                        $scope.selectedLng = point.lng +',' +point.lat + '';
                    }
                });
                G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

                setPlace();
            });

            function setPlace(){// 创建地址解析器实例
                var myGeo = new BMap.Geocoder();// 将地址解析结果显示在地图上,并调整地图视野
                myGeo.getPoint(myValue, function(point){
                    if (point) {
                        $scope.map.centerAndZoom(point, 17);
                        $scope.map.addOverlay(new BMap.Marker(point));
                    }
                }, "上海");
            }

            //添加点击地图事件
            $scope.map.addEventListener("click",showInfo);
        }
        //根据经纬度进行逆地址解析
        function revert_address() {
            if (items.longitude) {
                var lng = items.longitude.split(',')[0];
                var lat = items.longitude.split(',')[1];
                var geoc = new BMap.Geocoder();
                var pt = new BMap.Point(lng,lat);
                geoc.getLocation(pt, function(rs){
                    var addComp = rs.addressComponents;
                    console.log(addComp);
                    $scope.addComp = addComp;
                    var address;
                    if (addComp.province == addComp.city) {
                        address = addComp.province + addComp.district + addComp.street + addComp.streetNumber;
                    }else{
                        address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
                    }
                    angular.element('#address').val(address);
                });
            }
        }
        function showInfo(e){
            var point,
                marker;
            deletePoint();
            point = new BMap.Point(e.point.lng,e.point.lat);  //向地图添加标注
            marker = new BMap.Marker(point);
            $scope.map.addOverlay(marker);
            var geoc = new BMap.Geocoder();
            var pt = new BMap.Point(e.point.lng,e.point.lat);
            geoc.getLocation(pt, function(rs){
                var addComp = rs.addressComponents;
                var address;
                if (addComp.province == addComp.city) {
                    address = addComp.province + addComp.district + addComp.street + addComp.streetNumber;
                }else{
                    address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
                }
                angular.element('#address').val(address);
            });
        }

        //删除地图节点
        function deletePoint(){
            var allOverlay = $scope.map.getOverlays();
            for (var i = 0; i < allOverlay.length; i++){
                $scope.map.removeOverlay(allOverlay[i]);
            }
        }

        //获取分组信息
        $rootScope.getGroupInfo = function () {
            $.ajax({
                method:'GET',
                url:rootHttp + '/card/group/list',
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.groupInfo = data.data;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        };
        $rootScope.getGroupInfo();
        //填充弹框数据
        $scope.iccid = items.iccId;
        angular.element('#remark').val(items.remark);
        angular.element('#cardKey').val(items.keyWord);
        revert_address();
        $scope.cancel = function () {
            $rootScope.cardChangemodal.close();
        };
        $scope.ok = function () {
            var data = {
                iccId: items.iccId
            };
            var addressVal = angular.element('#address').val();
            if ($scope.selectedGroup) {
                data.groupId = $scope.selectedGroup.id
            }
            if (angular.element('#remark').val()) {
                data.remark = angular.element('#remark').val();
            }
            if (angular.element('#cardKey').val()) {
                data.KeyWord = angular.element('#cardKey').val();
            }
            if (angular.element('#address').val()) {
                data.address = angular.element('#address').val();
            }
            if ($scope.addressCode) {
                data.addressCode = $scope.addressCode;
            }

            if ($scope.selectedProvince && $scope.selectedCity == undefined) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择市';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            if ($scope.selectedProvince && $scope.selectedCity && $scope.selectedDstrict == undefined) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择区';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            if ($rootScope.compareDstrict && addressVal.indexOf($rootScope.compareDstrict) === -1) {
                console.log($rootScope.compareDstrict)
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请输入' + $rootScope.compareDstrict + '内的详细地址';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }

            if (angular.element('#address').val()) {
                $scope.myGeo.getPoint(angular.element('#address').val(), function(point){
                    if (point) {
                        $scope.selectedLng = point.lng + ',' +  point.lat;
                        data.longitude = $scope.selectedLng;
                        confirm();
                    }else{
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '请输入有效地址';
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }
                });
            }else{
                confirm();
            }
            function confirm () {
                $.ajax({
                    method:'POST',
                    url:rootHttp + '/card/update',
                    type:'json',
                    data: data,
                    cache: false,
                    success: function () {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '修改成功';
                                $rootScope.cardChangemodal.close();
                                var listData = {
                                    pageNo: 1,
                                    pageLimit: 10
                                };
                                $.ajax({
                                    method:'POST',
                                    url:rootHttp + '/card/list',
                                    type:'json',
                                    data:listData,
                                    cache: false,
                                    success: function(data){
                                        $rootScope.reportData = data.data.list;
                                        $scope.$apply();
                                    },
                                    error: function (data) {
                                        // error
                                    }
                                });
                                var mapData = {
                                    pageNo: 1,
                                    pageLimit: 5
                                };
                                $.ajax({
                                    method:'POST',
                                    url:rootHttp + '/card/map/list',
                                    type:'json',
                                    data: mapData,
                                    cache: false,
                                    success: function(data){
                                        $rootScope.cardFindData = data.data.list;
                                        $scope.$apply();
                                    },
                                    error: function (data) {
                                        // error
                                    }
                                });
                                if ($location.hash() === 'cardManage') {
                                    var lng = $rootScope.cardFindData[0].longitude;
                                    $scope.map = new BMap.Map("map",{enableMapClick:false});// 创建Map实例
                                    var point = new BMap.Point(lng.split(',')[0],lng.split(',')[1]);
                                    var marker = new BMap.Marker(point);
                                    $scope.map.addOverlay(marker);
                                    $scope.map.centerAndZoom(point, 13);// 初始化地图,设置中心点坐标和地图级别
                                    var opts = {type: BMAP_NAVIGATION_CONTROL_SMALL,anchor: BMAP_ANCHOR_TOP_RIGHT};
                                    $scope.map.addControl(new BMap.NavigationControl(opts));
                                    $scope.map.setCurrentCity("上海");          // 设置地图显示的城市 此项是必须设置的
                                    $scope.map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
                                }
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    },
                    error: function (data) {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = data.returnCode;
                                $rootScope.cardChangemodal.close();
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }
                });
            }

        };

        // 根据省市区代码表填充已选数据

        init_data();
        function init_data () {
            var i = 0,
                j = 0,
                k = 0;
            $http.get('js/json/country.json').then(function (res) {
                $scope.initData = res.data;
                $scope.initCity = [];
                $scope.initDistricts = [];
                for (i;i < $scope.initData.length; i++) {
                    var provinceStr = $scope.initData[i].cityId + '';
                    var dataCur = items.addressCode + '';
                    if (provinceStr.substr(0,2) == dataCur.substr(0,2)) {
                        $scope.initCity = $scope.initData[i].children;
                        $scope.selectedProvince = $scope.initData[i].cityId;
                        for (j; j < $scope.initCity.length; j++) {
                            var cityStr = $scope.initCity[j].cityId + '';
                            if(cityStr.substr(2,2) == dataCur.substr(2,2)) {
                                $scope.initDistricts = $scope.initCity[j].children;
                                $scope.selectedCity = $scope.initCity[j].cityId;
                                for (k; k < $scope.initDistricts.length;k++) {
                                    var districtStr = $scope.initDistricts[k].cityId + '';
                                    if(districtStr.substr(4,2) == dataCur.substr(4,2)) {
                                        $scope.selectedDstrict = $scope.initDistricts[k].cityId;
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }


        // 三级联动设置
        $http.get('js/json/country.json').then(function (res) {
            $scope.regionData = res.data;
            $scope.citys = [];
            $scope.districts = [];
            $scope.regionText = {};
            $scope.$watch('selectedProvince',function(newValue,oldValue){
                var i = 0,len = $scope.regionData.length;
                if(!newValue){ //判断选择的是否选择省份，如果没有则重置市区
                    $scope.citys = [];
                    $scope.districts = [];
                    return;
                }
                for(i;i < len;i ++){
                    if($scope.regionData[i].cityId == $scope.selectedProvince){
                        $scope.citys = $scope.regionData[i].children;
                        $scope.regionText.selectedProvinceText = $scope.regionData[i].name;
                        if (newValue != oldValue) {
                            $scope.map.centerAndZoom($scope.regionData[i].name,17);
                        }
                    }
                }
                $scope.districts = [];
            });

            $scope.$watch('selectedCity',function(newValue,oldValue){
                if(!newValue){ //作用同上
                    $scope.districts = [];
                    return;
                }
                var i = 0,len = $scope.citys.length;
                for(i;i < len; i ++){
                    if($scope.citys[i].cityId == $scope.selectedCity){
                        $scope.districts = $scope.citys[i].children;
                        $scope.regionText.selectedCityText = $scope.citys[i].name;
                        if (newValue != oldValue) {
                            $scope.map.centerAndZoom($scope.citys[i].name,17);
                        }
                    }
                }
            });
            $scope.districtObj = {};
            $scope.$watch('selectedDstrict',function(newValue,oldValue){
                if(!newValue){ //作用同上
                    $scope.districts = [];
                    $rootScope.compareDstrict = null;
                    $scope.addressCode = null;
                    return;
                }
                var i = 0,len = $scope.districts.length;
                for(i;i < len ; i ++){
                    if($scope.districts[i].cityId == $scope.selectedDstrict){
                        $scope.districtObj = $scope.districts[i];
                        $scope.regionText.selectedDstrictText = $scope.districts[i].name;
                        $rootScope.compareDstrict = $scope.districts[i].name;
                        $scope.addressCode = $scope.districts[i].cityId;
                        if (newValue != oldValue) {
                            $scope.map.centerAndZoom($scope.districts[i].name,17);
                        }
                    }
                }
            });
        });


        //新建分组
        $scope.newGroup = function () {
            $rootScope.newGroupmodal = $uibModal.open({
                templateUrl: 'html/groupTemplate.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                controller: 'groupAddController',
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        //管理分组
        $scope.manageGroup = function () {
            $rootScope.manGroupmodal = $uibModal.open({
                templateUrl: 'html/groupManage.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                size: 'middle'//模态框的大小尺寸
            });

        };

    }])
    .controller('groupAddController',['$scope','$rootScope','$http','$uibModal',function($scope,$rootScope,$http,$uibModal){
        $scope.close = function () {
            $rootScope.newGroupmodal.close();
        };
        $scope.cancel = function () {
            $rootScope.newGroupmodal.close();
        };
        $scope.title = '新建物联网卡分组';
        $scope.submit = function () {
            var groupName = angular.element('#groupName').val();
            var data = {
                name: groupName
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/group/save',
                type:'json',
                data: data,
                cache: false,
                success: function(data){
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '新增成功';
                            $rootScope.newGroupmodal.close();
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    $rootScope.getGroupInfo();
                },
                error: function (data) {
                    // error
                }
            })
        }
    }])
    .controller('groupModifyController',['$scope','$rootScope','$http','$uibModal',function($scope,$rootScope,$http,$uibModal){
        $scope.close = function () {
            $rootScope.modifyGroupmodal.close();
        };
        $scope.cancel = function () {
            $rootScope.modifyGroupmodal.close();
        };
        $scope.title = '修改物联网卡分组';
        $scope.groupItemName = $rootScope.groupItemModify.name;
        $scope.submit = function () {
            var groupName = angular.element('#groupName').val();
            var data = {
                id: $rootScope.groupItemModify.id,
                name: groupName
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/group/update',
                type:'json',
                data: data,
                cache: false,
                success: function(data){
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '修改成功';
                            $rootScope.modifyGroupmodal.close();
                            $.ajax({
                                method:'GET',
                                url:rootHttp + '/card/group/list',
                                type:'json',
                                cache: false,
                                success: function(data){
                                    $rootScope.groups = data.data;
                                    $rootScope.groupInfo = data.data;
                                    $scope.$apply();
                                },
                                error: function (data) {
                                    // error
                                }
                            });
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                },
                error: function (data) {
                    // error
                }
            })
        }
    }])
    .controller('groupManageController',['$scope','$rootScope','$http','$uibModal',function($scope,$rootScope,$http,$uibModal){
        $.ajax({
            method:'GET',
            url:rootHttp + '/card/group/list',
            type:'json',
            cache: false,
            success: function(data){
                $rootScope.groups = data.data;
                $scope.$apply();
            },
            error: function (data) {
                // error
            }
        });
        $scope.close = function () {
            $rootScope.manGroupmodal.close();
        };
        $scope.cancel = function () {
            $rootScope.manGroupmodal.close();
        };
        $scope.modify = function (items) {
            $rootScope.groupItemModify = items;
            $rootScope.modifyGroupmodal = $uibModal.open({
                templateUrl: 'html/groupTemplate.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                controller: 'groupModifyController',
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false
            });
        };
        $scope.delete = function (items) {
            $rootScope.groupItemDelete = items;
            $rootScope.confirmmodal = $uibModal.open({
                templateUrl: 'html/confirmTemplate.html',
                controller: 'groupDelConfirmController'
            });
        }
    }])
    .controller('cardMessageController',['$scope','$rootScope','$http','$uibModal',function($scope,$rootScope,$http,$uibModal){
        var items = JSON.parse(sessionStorage.getItem('cardMessage'));
        $scope.iccid = items.iccId;
        $scope.cancel = function () {
            $rootScope.cardMessagemodal.close();
        };
        $scope.submit = function () {
            var upWord = angular.element('#upWord').val();
            var data = {
                iccId: items.iccId,
                content: upWord
            };
            $.ajax({
                method: 'POST',
                url: rootHttp + '/card/upwardMessage',
                type:'json',
                data:data,
                cache:false,
                success: function (data) {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = data.returnCode;
                            $rootScope.cardMessagemodal.close();
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                },
                error: function () {
                    // error
                }
            });
        };
    }])
    .controller('groupDelConfirmController',['$scope','$rootScope','$http','$uibModal',function($scope,$rootScope,$http,$uibModal){
        $scope.confirm = '确认删除？';
        $scope.cancel = function () {
            $rootScope.confirmmodal.close();
        };
        $scope.ok = function(){
            var groupId = $rootScope.groupItemDelete;
            var data = {
                id: groupId.id
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/group/delete',
                type:'json',
                data: data,
                cache: false,
                success: function(){
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl: 'html/hintTemplate.html',
                        controller: function($scope,$rootScope){
                            $scope.confirm = '删除成功';
                            $rootScope.confirmmodal.close();
                            $.ajax({
                                method:'GET',
                                url:rootHttp + '/card/group/list',
                                type:'json',
                                cache: false,
                                success: function(data){
                                    $rootScope.groups = data.data;
                                    $scope.$apply();
                                },
                                error: function (data) {
                                    // error
                                }
                            });
                            $scope.ok = function () {
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                    $rootScope.getGroupInfo();
                },
                error: function (data) {
                    // error
                }
            });

        }
    }])
    //code by liuqixing

    .controller("phoneManageController",['$scope','$http','$location','$log','$uibModal','$rootScope',function($scope,$http,$location,$log,$uibModal,$rootScope){
        $scope.id = $location.search().id;

        // 分页功能
        $scope.maxSize = 3;       //可选页数范围
        $scope.currentPage = 1;   //当前页数
        $rootScope.phoneTotalItems = 0;    //总页数
        $scope.perPage = 10;      //每页显示多少条
        $.ajax({
            url:rootHttp + '/card/phone/list',
            method:"POST",
            dataType:"json",
            data:{
                iccId:$scope.id,
                pageNo:1,
                pageLimit:10
            },
            success:function(data){
                $rootScope.phoneData = data.data.list;
                $rootScope.phoneTotalItems = data.data.dataCount;
                $scope.$apply();
            },
            error:function(data){
                // err
            }
        });
        $scope.pageChanged = function () {
            var data = {
                iccId:$scope.id,
                pageNo: $scope.currentPage,
                pageLimit: $scope.perPage
            };
            if ($scope.queryMode == 'phone') {
                $scope.queryValue ? data.phone = $scope.queryValue : null;
            }else if ($scope.queryMode == 'name') {
                $scope.queryValue ? data.username = $scope.queryValue : null;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/card/phone/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.phoneData = data.data.list;
                    $rootScope.phoneTotalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };
        $scope.queryMode = 'phone';
        //搜索功能
        $scope.search = function () {
            var data = {
                iccId: $scope.id,
                pageNo: 1,
                pageLimit: 10
            };
            if ($scope.queryMode == 'phone') {
                $scope.queryValue ? data.phone = $scope.queryValue : null;
            }else if ($scope.queryMode == 'name') {
                $scope.queryValue ? data.username = $scope.queryValue : null;
            }
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/phone/list',
                type:'json',
                data:data,
                cache: false,
                success: function(data){
                    $rootScope.phoneData = data.data.list;
                    $scope.phoneTotalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        };

        /*
         *   全选单选
         * */
        $scope.count = 0;//已选择数量
        $scope.selectData = [];//已选对象


        //选择单个（取消选择单个
        $scope.changeCurrent = function(current, $event) {
            //计算已选数量 true加， false减
            $scope.count += current.checked ? 1 : -1;
            //判断是否全选，选数量等于数据长度为true
            $scope.selectAll = $scope.count === $rootScope.phoneData.length;
            //统计已选对象
            $scope.selectData = [];
            angular.forEach($rootScope.phoneData, function(item) {
                if(item.checked){
                    $scope.selectData[$scope.selectData.length] = item;
                }
            });
            console.log($scope.selectData)
            $event.stopPropagation();//阻止冒泡

        };
        //单击行选中
        // $scope.changeCurrents = function(current, $event) {
        //     if(current.checked == undefined){
        //         current.checked = true;
        //     }else{
        //         current.checked = !current.checked;
        //     }
        //     $scope.changeCurrent(current, $event);
        //
        // };
        //全选（取消全选
        $scope.changeAll = function() {

            $rootScope.selectAll = $scope.selectAll;

            angular.forEach($rootScope.phoneData, function(item) {
                item.checked = $scope.selectAll;
            });

            $scope.count = $scope.selectAll ? $rootScope.phoneData.length : 0;
            if ($scope.selectAll) {
                $scope.selectData = $rootScope.phoneData;
            } else {
                $scope.selectData = [];
            }
        };

        //delete All
        $scope.deleteAll = function(){
            var ids = [];
            for (var i = 0;i < $scope.selectData.length;i++) {
                var idItem = {};
                idItem.id = $scope.selectData[i].id;
                ids.push(idItem);
            }
            var data = {
                ids: ids
            };
            if ($scope.selectData.length == 0) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl: 'html/hintTemplate.html',
                    controller: function($scope,$rootScope){
                        $scope.confirm = '请选择数据';
                        $scope.ok = function () {
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/phone/delete',
                type:'json',
                cache: false,
                data: {
                    "ids": JSON.stringify(data)
                },
                success: function (res) {
                    if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '删除成功';
                                $.ajax({
                                    method:'GET',
                                    url:rootHttp + '/card/phone/list',
                                    type:'json',
                                    data:{
                                        iccId:$location.search().id,
                                        pageNo:1,
                                        pageLimit:10
                                    },
                                    cache: false,
                                    success: function(data){
                                        $rootScope.phoneData = data.data.list;
                                        $rootScope.phoneTotalItems = data.data.dataCount;
                                        $scope.$apply();
                                    },
                                    error: function (data) {
                                        // error
                                    }
                                });
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $scope.selectData = [];
                    }else if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '删除失败';
                                $scope.selectData = [];
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $scope.selectData = [];
                    }
                }
            });
        };



        //修改事件
        $scope.zdTableEdit = function(item, $event){
            $event.stopPropagation();//阻止冒泡
            $rootScope.dataItems = item;



            // 修改弹出框
            $rootScope.zdTableEditmodal = $uibModal.open({
                templateUrl: 'html/ManagementMobile.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                controller: 'zdTableEditController',
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false,
                resolve: {//这是一个入参,这个很重要,它可以把主控制器中的参数传到模态框控制器中
                    items: function () {//items是一个回调函数

                        return $scope.item;//这个值会被模态框的控制器获取到
                    }
                }
            });
            $rootScope.zdTableEditmodal.result.then(function (item) {//这是一个接收模态框返回值的函数
                //console.log(item)//模态框的返回值
            }, function (){
                // error
            });

        };
        /*
         * 导出
         *   TODO .....
         *
         * */

        $scope.export = function(){
            window.location.href = rootHttp + '/card/exportPhone';
        }
        $scope.downloadTemp = function () {
            window.location.href = '/js/importTemplate/phoneTemplate.xlsx'
        };

        //删除事件
        $scope.zdTableRemove = function(items){
            $rootScope.phoneItemDelete = items;
            $rootScope.confirmmodal = $uibModal.open({
                templateUrl: 'html/confirmTemplate.html',
                controller: 'phoneDelConfirmController'
            });
        };
        /*
         * 批量导入窗口
         * */
        $scope.bulkImport = function(){
            $rootScope.bulkImportmodal = $uibModal.open({
                templateUrl:'html/bulkImport.html',
                controller:'bulkImportController',
                size:'middle',
                backdrop:'static',
                keyboard:false
            });
        };

        //新增用户
        $scope.newAdd = function(){
            //新增弹出窗口
            $rootScope.newAddmodal = $uibModal.open({
                templateUrl:'html/ManagementMobile.html',
                controller:'newAddController',
                size:'middle',
                backdrop:'static',
                keyboard:false
            });
        };
    }])

    .controller("zdTableEditController",['$scope','$http','$rootScope','$location','$uibModal',function($scope,$http,$rootScope,$location,$uibModal) {
        //点击修改按钮传入当前行内数据
        $scope.itemsConTel = $rootScope.dataItems.phone;
        $scope.itemsConName = $rootScope.dataItems.username;
        $scope.itemsConID = $rootScope.dataItems.identityId;
        $scope.tabOne = true;
        $scope.tabTwo = false;
        $scope.tab2 = function(){
            $scope.tabOne = false;
            $scope.tabTwo = true;
        };
        $scope.tab1 = function(){
            $scope.tabOne = true;
            $scope.tabTwo = false;
        };
        $scope.submit = function () {
            var data = {
                id:  $rootScope.dataItems.id,
                iccId: $rootScope.dataItems.iccId,
                phone: angular.element('#managePhone').val(),
                username: angular.element('#manageName').val()

            };
            var pattern = /^1[3|4|5|8][0-9]\d{4,8}$/;
            if (!pattern.test(angular.element('#managePhone').val())) {
                console.log(angular.element('#managePhone').val());
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl:'html/hintTemplate.html',
                    controller:function($scope,$rootScope){
                        $scope.confirm = '请输入正确的手机号';
                        $scope.ok = function(){
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            if (!angular.element('#manageName').val()) {
                angular.element('#manageName').val()
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl:'html/hintTemplate.html',
                    controller:function($scope,$rootScope){
                        $scope.confirm = '请输入机主姓名';
                        $scope.ok = function(){
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/phone/update',
                type:'json',
                data: data,
                cache: false,
                success: function(res){
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl:'html/hintTemplate.html',
                        controller:function($scope,$rootScope){
                            $scope.confirm = '修改成功';
                            $rootScope.zdTableEditmodal.close();
                            $scope.id = $location.search().id;
                            $.ajax({
                                url:rootHttp + '/card/phone/list',
                                method:"POST",
                                dataType:"json",
                                data:{
                                    iccId:$scope.id,
                                    pageNo:1,
                                    pageLimit:10
                                },
                                success:function(data){
                                    $rootScope.phoneData = data.data.list;
                                    $scope.$apply();
                                },
                                error:function(data){
                                    // err
                                }
                            });
                            $scope.ok = function(){
                                $rootScope.hintmodal.close();
                            }
                        }
                    });
                }
            });
        };
        $scope.close = function(){
            $rootScope.zdTableEditmodal.close();
        };
        $scope.cancel = function (){
            $rootScope.zdTableEditmodal.close();
        };
    }])
    .controller('newAddController',['$scope','$rootScope','$http','$uibModal','$location',function($scope,$rootScope,$http,$uibModal,$location){
        $scope.tabOne = true;
        $scope.tabTwo = false;
        $scope.tab2 = function(){
            $scope.tabOne = false;
            $scope.tabTwo = true;
        };
        $scope.tab1 = function(){
            $scope.tabOne = true;
            $scope.tabTwo = false;
        };
        $scope.close = function(){
            $rootScope.newAddmodal.close();
        };
        $scope.cancel = function(){
            $rootScope.newAddmodal.close();
        } ;

        //确认弹窗

        //新增用户电话信息
        $scope.submit = function(){
            var pattern = /^1[3|4|5|8][0-9]\d{4,8}$/;
            if (!pattern.test($scope.conTel)) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl:'html/hintTemplate.html',
                    controller:function($scope,$rootScope){
                        $scope.confirm = '请输入正确的手机号';
                        $scope.ok = function(){
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            if (!$scope.conName) {
                $rootScope.hintmodal = $uibModal.open({
                    templateUrl:'html/hintTemplate.html',
                    controller:function($scope,$rootScope){
                        $scope.confirm = '请输入机主姓名';
                        $scope.ok = function(){
                            $rootScope.hintmodal.close();
                        }
                    }
                });
                return;
            }
            $.ajax({
                url:rootHttp + '/card/phone/save',
                method:"POST",
                data:{
                    iccId:$location.search().id,
                    phone:$scope.conTel,
                    username:$scope.conName,
                    identityId:$scope.conID,
                    address:$scope.address
                }
            }).success(function(data){
                if(data.returnCode == '200'){
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl:'html/hintTemplate.html',
                        controller:function($scope,$rootScope){
                            $scope.confirm = '新增成功';
                            $rootScope.newAddmodal.close();
                            $.ajax({
                                method:'GET',
                                url:rootHttp + '/card/phone/list',
                                type:'json',
                                data:{
                                    iccId:$location.search().id,
                                    pageNo:1,
                                    pageLimit:10
                                },
                                cache: false,
                                success: function(data){
                                    $rootScope.phoneData = data.data.list;
                                    $rootScope.phoneTotalItems = data.data.dataCount;
                                    $scope.$apply();
                                },
                                error: function (data) {
                                    // error
                                }
                            });
                            $scope.ok = function(){
                                $rootScope.hintmodal.close();
                            }
                        }
                    }) ;
                }else {
                    $rootScope.hintmodal = $uibModal.open({
                        templateUrl:'html/hintTemplate.html',
                        controller:function($scope,$rootScope){
                            $scope.confirm = data.returnMsg;
                            $scope.ok = function(){
                                $rootScope.hintmodal.close();
                            }
                        }
                    }) ;
                }

            }).error(function(){

            });
        };
    }])

    .controller('bulkImportController',['$scope','$log','$rootScope','$uibModal','$location',function($scope,$log,$rootScope,$uibModal,$location){
        $scope.close = function(){
            $rootScope.bulkImportmodal.close();
        };
        $scope.cancel = function(){
            $rootScope.bulkImportmodal.close();
        };
        $scope.importTitle = '注意：批量导入将覆盖通讯方式和手机号都相同的历史数据!';
        $scope.submit = function (){
            var form = new FormData(angular.element("#fileImport")[0]);
            $.ajax({
                url: rootHttp + '/card/import-phone',
                type: 'POST',
                data: form,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success:function(data){
                    if (data.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '上传成功';
                                $rootScope.bulkImportmodal.close();
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (data.returnCode == '499'){
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '上传失败';
                                $rootScope.bulkImportmodal.close();
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }

                },
                error:function(data){
                    console.log(data)
                }
            });
        }
    }])
    .controller('cardDetailController',['$scope','$http','$location',function($scope,$http,$location){
        $.ajax({
            url:rootHttp + '/card/detail',
            method:"POST",
            dataType:"json",
            data:{
                'simId':$location.search().id
            },
            success:function(data){
                var tempData = data.data;
                $scope.acctManagementSystemId = tempData.acctManagementSystemId;
                $scope.iccId = tempData.iccId;
                $scope.msiSdn = tempData.msiSdn;
                $scope.imsi1 = tempData.imsi1;
                $scope.imei = tempData.imei;
                $scope.status = tempData.status;
                $scope.activeDate = tempData.activeDate;
                $scope.online = tempData.online;
                $scope.address = tempData.address;
                $scope.longitude = tempData.longitude;
                $scope.$apply();
            },
            error:function(data){
                // err
            }
        });
        $scope.goBack = function () {
            $location.url('/cardManage');
        }

    }])
    .controller('phoneDelConfirmController',['$scope','$rootScope','$http','$uibModal','$location',function($scope,$rootScope,$http,$uibModal,$location){
        $scope.confirm = '确认删除？';
        $scope.cancel = function () {
            $rootScope.confirmmodal.close();
        };
        $scope.ok = function(){
            var data = {
                ids: [{
                    "id":$rootScope.phoneItemDelete.id
                }]
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/phone/delete',
                type:'json',
                cache: false,
                data: {
                    ids:JSON.stringify(data)
                },
                success: function (res) {
                    if (res.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '删除成功';
                                $rootScope.confirmmodal.close();
                                $.ajax({
                                    method:'GET',
                                    url:rootHttp + '/card/phone/list',
                                    type:'json',
                                    data:{
                                        iccId:$rootScope.phoneItemDelete.iccId,
                                        pageNo:1,
                                        pageLimit:10
                                    },
                                    cache: false,
                                    success: function(data){
                                        $rootScope.phoneData = data.data.list;
                                        $rootScope.phoneTotalItems = data.data.dataCount;
                                        $scope.$apply();
                                    },
                                    error: function (data) {
                                        // error
                                    }
                                });
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (res.returnCode == '499') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl: 'html/hintTemplate.html',
                            controller: function($scope,$rootScope){
                                $scope.confirm = '删除失败';
                                $rootScope.confirmmodal.close();
                                $scope.ok = function () {
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }
                }
            });
        }
    }])
    .controller('errorDataController',['$scope','$rootScope','$uibModal',function($scope,$rootScope,$uibModal){

        function cardList () {
            var data = {
                pageNo: 1,
                pageLimit: 10,
                type: 0
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/import/error/list',
                type:'json',
                data: data,
                cache: false,
                success: function(data){
                    $scope.cardErrorData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        }
        function phoneList () {
            var dataOther = {
                pageNo: 1,
                pageLimit: 10,
                type: 1
            };
            $.ajax({
                method:'POST',
                url:rootHttp + '/card/import/error/list',
                type:'json',
                data: dataOther,
                cache: false,
                success: function(data){
                    $scope.phoneErrorData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function (data) {
                    // error
                }
            });
        }
        cardList();
        // 物联网卡分页
        $scope.maxSize = 3;       //可选页数范围
        $scope.currentPage = 1;   //当前页数
        $scope.totalItems = 0;    //总页数
        $scope.perPage = 10;      //每页显示多少条

        $scope.pageChanged = function () {
            var data = {pageNo: $scope.currentPage,pageLimit: $scope.perPage,type: 0};
            $.ajax({
                method: 'POST',
                url:rootHttp + 'card/import/error/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $scope.cardErrorData = data.data.list;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };
        // 手机分页
        $scope.maxSizeOther = 3;       //可选页数范围
        $scope.currentPageOther = 1;   //当前页数
        $scope.totalItemsOther = 0;    //总页数
        $scope.perPageOther = 10;      //每页显示多少条

        $scope.pageChangedOther = function () {
            var data = {pageNo: $scope.currentPageOther,pageLimit: $scope.perPageOther,type: 1};
            $.ajax({
                method: 'POST',
                url:rootHttp + 'card/import/error/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $scope.phoneErrorData = data.data.list;
                    $scope.totalItemsOther = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };

        $scope.downloadCard = function (items) {
            window.location.href = rootHttp + '/card/downLoadFile?mediaId=' +items.mediaId+"&filename="+items.fileName;
        };
        $scope.downloadPhone = function (items) {
            window.location.href = rootHttp + '/card/downLoadFile?mediaId='+items.mediaId+"&filename="+items.fileName;
        };
        // 点击tab请求数据
        $scope.tabCard = function () {
            cardList();
        };
        $scope.tabPhone = function () {
            phoneList();
        }
    }])
    //物联网卡批量绑定
    .controller('tempBindController',['$scope','$rootScope','$uibModal',function($scope,$rootScope,$uibModal){
        $scope.close = function(){
            $rootScope.tempBindmodal.close();
        };
        $scope.cancel = function(){
            $rootScope.tempBindmodal.close();
        };
        $scope.importTitle = '注意：批量绑定将覆盖物联网卡ID相同的历史数据！未匹配到物联网卡ID的新信息将不会导入！';
        $scope.submit = function (){
            var form = new FormData(angular.element("#fileImport")[0]);
            $.ajax({
                url: rootHttp + '/card/import-card',
                type: 'POST',
                data: form,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'json',
                success:function(data){
                    if (data.returnCode == '200') {
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '上传成功';
                                $rootScope.tempBindmodal.close();
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }else if (data.returnCode == '499'){
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '上传失败';
                                $rootScope.tempBindmodal.close();
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                    }

                },
                error:function(data){
                    console.log(data)
                }
            });
        }
    }])
    .controller('sendMessageController',['$scope','$rootScope','$uibModal',function($scope,$rootScope,$uibModal){
        angular.element('#loading').show();
        var data = {
            pageNo: 1,
            pageLimit: 10
        };
        $.ajax({
            method:'POST',
            url:rootHttp + '/rule/list',
            type:'json',
            data: data,
            cache: false,
            success: function(data){
                angular.element('#loading').hide();
                $rootScope.messageData = data.data.data;
                $scope.totalItems = data.data.dataCount;
                $scope.$apply();
            },
            error: function (data) {
                // error
            }
        });

        $scope.selectedId = 'all';
        //分页
        $scope.maxSize = 3;       //可选页数范围
        $scope.currentPage = 1;   //当前页数
        $scope.totalItems = 0;    //总页数
        $scope.perPage = 10;      //每页显示多少条

        $scope.pageChanged = function () {
            var data = {
                pageNo: $scope.currentPage,
                pageLimit: $scope.perPage
            };
            if ($scope.selectedId === 'active') {
                data.status = true;
            } else if ($scope.selectedId === 'stop') {
                data.status = false;
            }
            if ($scope.ruleName) {
                data.name = $scope.ruleName;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/rule/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.messageData = data.data.data;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        };

        //搜索按钮事件
        $scope.search = function () {
            var data = {
                pageNo: 1,
                pageLimit: 10
            };
            if ($scope.selectedId === 'active') {
                data.status = true;
            } else if ($scope.selectedId === 'stop') {
                data.status = false;
            }
            if ($scope.ruleName) {
                data.name = $scope.ruleName;
            }
            $.ajax({
                method: 'POST',
                url:rootHttp + '/rule/list',
                data:data,
                type:'json',
                cache: false,
                success: function(data){
                    $rootScope.messageData = data.data.data;
                    $scope.totalItems = data.data.dataCount;
                    $scope.$apply();
                },
                error: function(){
                    //error
                }
            });
        }
        // 激活/停用事件
        $scope.changeStatus = function (items) {
            var data = {
                id: items.pushApiEventSubId
            };
            if (items.enabled) {
                $.ajax({
                    method: 'POST',
                    url:rootHttp + '/rule/disable',
                    data:data,
                    type:'json',
                    cache: false,
                    success: function(data){
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '禁用成功';
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method: 'POST',
                            url:rootHttp + '/rule/list',
                            data:{
                                pageNo: 1,
                                pageLimit: 10
                            },
                            type:'json',
                            cache: false,
                            success: function(data){
                                $rootScope.messageData = data.data.data;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function(){
                                //error
                            }
                        });
                    }
                });
            }else{
                $.ajax({
                    method: 'POST',
                    url:rootHttp + '/rule/enable',
                    data:data,
                    type:'json',
                    cache: false,
                    success: function(data){
                        $rootScope.hintmodal = $uibModal.open({
                            templateUrl:'html/hintTemplate.html',
                            controller:function($scope,$rootScope){
                                $scope.confirm = '激活成功';
                                $scope.ok = function(){
                                    $rootScope.hintmodal.close();
                                }
                            }
                        });
                        $.ajax({
                            method: 'POST',
                            url:rootHttp + '/rule/list',
                            data:{
                                pageNo: 1,
                                pageLimit: 10
                            },
                            type:'json',
                            cache: false,
                            success: function(data){
                                $rootScope.messageData = data.data.data;
                                $scope.totalItems = data.data.dataCount;
                                $scope.$apply();
                            },
                            error: function(){
                                //error
                            }
                        });
                    }
                });
            }
        }

        //新建按钮事件
        $scope.add = function () {
            $rootScope.ruleManagemodal = $uibModal.open({
                templateUrl: 'html/ruleManage.html',//模态框的页面内容,这里的url是可以自己定义的,也就意味着什么都可以写
                size: 'middle',//模态框的大小尺寸
                backdrop: 'static',
                keyboard: false,
                controller: function ($scope,$rootScope) {
                    $scope.code = '106550010646';
                    $scope.cancel = function () {
                        $rootScope.ruleManagemodal.close();
                    };
                    $scope.active = function () {
                        if (!$scope.code) {
                            alert('请输入简短代码');
                            return;
                        }
                        if (!$scope.url) {
                            alert('请输入URL');
                            return;
                        }
                        if (!$scope.rule) {
                            alert('请输入命名规则');
                            return;
                        }
                        var data = {
                            code: $scope.code,
                            callbackUrl: $scope.url,
                            description: $scope.rule
                        };
                        $.ajax({
                            method: 'POST',
                            url:rootHttp + '/rule/create',
                            data:data,
                            type:'json',
                            cache: false,
                            success: function (res) {
                                if (res.returnCode == '499') {
                                    $rootScope.hintmodal = $uibModal.open({
                                        templateUrl:'html/hintTemplate.html',
                                        controller:function($scope,$rootScope){
                                            $scope.confirm = res.returnMsg;
                                            $scope.ok = function(){
                                                $rootScope.hintmodal.close();
                                            }
                                        }
                                    });
                                }else if (res.returnCode == '200') {
                                    $rootScope.hintmodal = $uibModal.open({
                                        templateUrl:'html/hintTemplate.html',
                                        controller:function($scope,$rootScope){
                                            $scope.confirm = '新建成功';
                                            $rootScope.ruleManagemodal.close();
                                            $.ajax({
                                                method: 'POST',
                                                url:rootHttp + '/rule/list',
                                                data:{
                                                    pageNo: 1,
                                                    pageLimit: 10
                                                },
                                                type:'json',
                                                cache: false,
                                                success: function(data){
                                                    $rootScope.messageData = data.data.data;
                                                    $scope.totalItems = data.data.dataCount;
                                                    $scope.$apply();
                                                },
                                                error: function(){
                                                    //error
                                                }
                                            });
                                            $scope.ok = function(){
                                                $rootScope.hintmodal.close();
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    };
                }
            });
        }
    }]);
