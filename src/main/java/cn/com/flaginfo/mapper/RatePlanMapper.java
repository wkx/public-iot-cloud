package cn.com.flaginfo.mapper;

import cn.com.flaginfo.model.domain.RatePlanDomain;
import cn.com.flaginfo.model.dto.RatePlanListDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@Mapper
public interface RatePlanMapper {

    void save(RatePlanDomain domain);

    int findPageCount(RatePlanListDTO dto);

    List<Map<String,Object>> findPage(RatePlanListDTO dto);

    List<Map<String,Object>> findAll(RatePlanListDTO dto);

}
