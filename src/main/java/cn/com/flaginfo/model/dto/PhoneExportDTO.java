package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/15.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class PhoneExportDTO {

    @NotNull(message = "物联网卡iccId不能为空")
    private String iccId;

    private String phone;

    private String username;

}
