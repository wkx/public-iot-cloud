package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class PhoneListInputDTO {

    @NotNull(message = "iccId不可为空")
    private String iccId;

    private String phone;

    private String username;

    private int index;

    @Min(value = 1,message = "pageNo最小为0")
    private int pageNo;

    @Min(value = 1,message = "pageLimit最小为1")
    private int pageLimit;
}
