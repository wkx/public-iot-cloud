package cn.com.flaginfo.controller;

import cn.com.flaginfo.common.support.JsonView;
import cn.com.flaginfo.common.support.SessionHolder;
import cn.com.flaginfo.model.domain.SmsTemplateDomain;
import cn.com.flaginfo.model.dto.SmsTemplateAddDTO;
import cn.com.flaginfo.model.dto.SmsTemplateDelDTO;
import cn.com.flaginfo.model.dto.SmsTemplateListDTO;
import cn.com.flaginfo.model.dto.SmsTemplateUpdateDTO;
import cn.com.flaginfo.service.SmsTemplateService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * Created by wkx on 2017/3/23.
 * @author wkx
 */
@RequestMapping("/smsTemplate")
@RestController
public class SmsTemplateController extends BaseController{

    @Autowired
    private SmsTemplateService smsTemplateService;

    private static final Logger LOGGER= LoggerFactory.getLogger(SmsTemplateController.class);

    @RequestMapping("/list")
    public JsonView list(@Valid SmsTemplateListDTO dto,BindingResult bResult){
        LOGGER.info("请求参数：{}", JSONObject.toJSON(dto));
        checkDTOParam(bResult);
        return null;
    }

    @RequestMapping("/add")
    public JsonView add(@Valid SmsTemplateAddDTO dto, BindingResult bResult, HttpSession session){
        LOGGER.info("请求参数：{}", JSONObject.toJSON(dto));
        checkDTOParam(bResult);
        String spId= SessionHolder.getSpId(session);
        SmsTemplateDomain domain=new SmsTemplateDomain();
        BeanUtils.copyProperties(dto,domain);
        domain.setSpId(spId);
        smsTemplateService.add(domain);
        return JsonView.getSuccView();
    }

    @RequestMapping("/update")
    public JsonView add(@Valid SmsTemplateUpdateDTO dto, BindingResult bResult){
        LOGGER.info("请求参数：{}", JSONObject.toJSON(dto));
        checkDTOParam(bResult);
        smsTemplateService.update(dto);
        return JsonView.getSuccView();
    }

    @RequestMapping("/delete")
    public JsonView delete(@Valid SmsTemplateDelDTO dto, BindingResult bResult){
        LOGGER.info("请求参数：{}", JSONObject.toJSON(dto));
        checkDTOParam(bResult);
        smsTemplateService.delete(dto);
        return JsonView.getSuccView();
    }
}
