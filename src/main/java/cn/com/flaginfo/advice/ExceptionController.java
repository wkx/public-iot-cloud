package cn.com.flaginfo.advice;

import cn.com.flaginfo.common.support.IotException;
import cn.com.flaginfo.common.support.JsonView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by wkx on 2017/3/6.
 * @author wkx
 */
@RestControllerAdvice
public class ExceptionController {

    private static final Logger LOGGER= LoggerFactory.getLogger(ExceptionController.class);

    @ExceptionHandler(IotException.class)
    public JsonView iotExceptionHandler(IotException ex){
        LOGGER.error(ex.getMessage(),ex);
        JsonView jsonView=new JsonView();
        jsonView.setReturnCode(ex.getCode());
        jsonView.setReturnMsg(ex.getMessage());
        return jsonView;
    }

    @ExceptionHandler(Exception.class)
    public JsonView exceptionHandler(Exception ex){
        LOGGER.info("程序出现未知异常");
        LOGGER.error(ex.getMessage(),ex);
        JsonView jsonView=new JsonView();
        jsonView.setFail();
        jsonView.setReturnMsg(ex.getMessage());
        return jsonView;
    }

}
