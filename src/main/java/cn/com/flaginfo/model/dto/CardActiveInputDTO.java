package cn.com.flaginfo.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * Created by wkx on 2017/3/14.
 * @author wkx
 */
@Data
@Accessors(chain = true)
public class CardActiveInputDTO {
    @NotNull(message = "simIdList不能为空")
    private String simIdList;
    @NotNull(message = "changeType不能为空")
    private int changeType;
    @NotNull(message = "targetValue不能为空")
    private String targetValue;
}
